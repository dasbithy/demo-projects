import Page from '../pages/base.page';
class CasLogin extends Page {
    //-------------------------Locators-------------------------------------------------x
    get _confirmationText() {
        return $("#hellpAnchor")
    }
    get _loginType() {
        return $("#usernameType")
    }
    get _userName() {
        return $("#usernameVisible")
    }
    get _password() {
        return $("#password")
    }
    get _domain() {
        return $("//select[@id='domain']")
    }
    get _submit() {
        return $("//input[@id='loginButton']")
    }
    get _logOutLink() {
        return $("//strong[contains(text(),'Logout')]")
    }
    get _ifNotAbleToLogIn() {
        return $("//li[contains(text(),'Sign-in failed for ap-master.swp. Invalid username or password')]")
    }
    get _searchBoxPrivate() {
        return $("//div[@id='privateSiteWikisTable_filter']//input[@type='search']")
    }
    get _newFeatureBox() {
        return $("//a[@class='introjs-button introjs-skipbutton']")
    }
    get _closeBtn() {
        return $(".introjs-skipbutton")
    }
    get _legacySignin() {
        return $("a.textbox:nth-child(1)")
    }
    get _assistPage() {
        return $(".assist")
    }
    get _myOffice() {
        return $("//font[@class='title']")
    }
    get _closeNewFeatureCheckBox() {
        return $("#manage-splash")
    }
    //-------------------------Locators-------------------------------------------------x

    open(path) {
        super.open(path)
    }


    WaitForPageToLoad() {
        return super.WaitForElementToBeLoaded(this._confirmationText)
    }

    SelectLoginType() {
        return super.ClickOn(this._loginType);
    }

    EnterUserName(username) {
        return super.EnterData(this._userName, username)
    }

    EnterPassword(password) {
        return super.EnterData(this._password, password)
    }

    SelectDomain(domain) {
        return super.SelectItem(this._domain, domain)
    }

    HitSubmit() {
        return super.ClickOn(this._submit)
    }

    RefreshifErrorComesUp() {
        if (this._ifNotAbleToLogIn.isDisplayed()) {
            browser.refresh()
        }
    }

    waitUntilPageLoadAfterLogin(impWait, pageLoad) {
        return browser.setTimeouts(impWait, pageLoad)
    }

    CloseNewFeatureWindow() {
        super.ClickOn(this._closeNewFeatureCheckBox)
        super.ClickOn(this._newFeatureBox)
    }

    SuccessfullLoggedIn() {
        this.CloseNewFeatureWindow()
        return super.GetTextFromElement(this._logOutLink)
    }

    NavigateToSigninPage() {
        if (this._assistPage.isDisplayed()) {
            return super.ClickOn(this._legacySignin)
        }
    }

    VerifyMyOfficeAppearsAfterLogin() {

        return super.GetTextFromElement(this._myOffice)
    }

}

export default new CasLogin()