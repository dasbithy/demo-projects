import Page from "./base.page";

class Browse extends Page {

    //-------------------------Locators-------------------------------------------------x  
    //Opportunity
    get _byOfficeRadio() {
        return $("[action] [align='left']:nth-of-type(1) .browseByCategory")
    }
    get _verifyByOffice() {
        return $("//form[@id='browseByOffice']")
    }
    get _recordsFoundByOffice() {
        return $("[action] table:nth-child(9) tr td:nth-of-type(3)")
    }
    get _recordsFoundByClientCompany() {
        return $("[action] table:nth-child(9) tr td:nth-of-type(2)")
    }
    get _byClientCompany() {
        return $("[action] [align='left']:nth-of-type(2) .browseByCategory")
    }
    get _verifyByClientCompany() {
        return $("//form[@id='browseByProspect']")
    }
    get _byIndustry() {
        return $("[action] [align='left']:nth-of-type(3) .browseByCategory")
    }
    get _verifyByIndustry() {
        return $("//form[@id='browseByIndustry']")
    }
    get _recordFoundByIndustry() {
        return $("[action] table:nth-child(9) td")
    }
    get _byArchive() {
        return $("[action] [align='left']:nth-of-type(4) .browseByCategory")
    }
    get _verifyByArchive() {
        return $("//form[@id='browseByArchived']")
    }
    get _recordFoundByArchive() {
        return $("[action] table:nth-child(11) tr td:nth-of-type(2)")
    }
    get _byRecentActivity() {
        return $("[action] [align='left']:nth-of-type(5) .browseByCategory")
    }
    get _verifyByRecentActivity() {
        return $("//form[@id='browseByRecentActivity']")
    }
    get _recordFoundByRecentActivity() {
        return $("//td[contains(text(),'Records found:')]")
    }
    //Client Companies
    get _clientCompanySearch() {
        return $("//input[@id='clientName']")
    }
    get _lobDrpDown() {
        return $("//select[@id='lobDropDown']")
    }
    get _searchBtn() {
        return $("//button[@id='searchImage']")
    }
    get _resultAppear() {
        return $(".data .browntxt")
    }
    //ClientContacts
    get _clientContactName() {
        return $("[name='searchCriteria\.name']")
    }
    //ActivityName
    get _activityName() {
        return $("[name='searchCriteria\.title']")
    }
    get _resutlsReturnCount() {
        return $("b")
    }
    get _exportFile() {
        return $("[title='Export to Excel']")
    }
    //-------------------------Locators-------------------------------------------------x

    Open(path) {
        super.open(path)

        let currentUrl = browser.getUrl()
        if(!currentUrl.includes("user/BrowseProspects.action")){
            browser.refresh()
            super.open(path)
        }
    }

    ClickOnByOffice() {
        return super.ClickOn(this._byOfficeRadio)

    }

    VerifyOpportunitiesByOffice() {

        return super.IsElementExist(this._verifyByOffice)
    }

    VerifyRecordsFoundByOffice() {
        let getRecord = this._recordsFoundByOffice.getText().split(" ")
        let result2 = parseInt(getRecord[2])
        return result2
    }

    ClickOnByClientCompany() {

        return super.ClickOn(this._byClientCompany)

    }

    VerifyOpportunitiesByClientCompany() {

        return super.IsElementExist(this._verifyByClientCompany)

    }

    VerifyRecrodsFoundByClientCompany() {
        let getRecord = this._recordsFoundByClientCompany.getText().split(" ")
        let result2 = parseInt(getRecord[2])
        return result2
    }

    ClickOnByIndustry() {

        return super.ClickOn(this._byIndustry)

    }

    VerifyOpportunitiesByIndustry() {

        return super.IsElementExist(this._verifyByIndustry)

    }

    VerifyRecrodsFoundByIndustry() {
        let getRecord = this._recordFoundByIndustry.getText().split(" ")
        let result2 = parseInt(getRecord[2])
        return result2
    }

    ClickOnByArchive() {

        return super.ClickOn(this._byArchive)

    }

    VerifyOpportunitiesByArchive() {

        return super.IsElementExist(this._verifyByArchive)

    }

    VerifyRecrodsFoundByArchive() {
        let getRecord = this._recordFoundByArchive.getText().split(" ")
        let result2 = parseInt(getRecord[2])
        return result2
    }

    ClickOnByRecentActivity() {

        return super.ClickOn(this._byRecentActivity)

    }

    VerifyOpportunitiesByRecentActivity() {

        return super.IsElementExist(this._byRecentActivity)

    }

    VerifyRecrodsFoundByRecentActivity() {
        let getRecord = this._recordFoundByRecentActivity.getText().split(" ")
        let result2 = parseInt(getRecord[2])
        return result2
    }

    //SearchByClientContact
    EnterClientCompanyName(name) {

        super.IsElementPresent(this._searchBtn)
        return super.EnterData(this._clientCompanySearch, name)

    }

    SelectLobFromDropDown(item) {

        return super.SelectItem(this._lobDrpDown, item)

    }

    ClickOnSearch() {

        return super.ClickOn(this._searchBtn)

    }

    VerifyClientCompanyAppearsBySearch() {

        return super.GetTextFromElement(this._resultAppear)

    }

    VerifyResultsCount() {

        let count = parseInt(super.GetTextFromElement(this._resutlsReturnCount))
        return count
    }

    //SearchByClientContacts
    EnterClientContactName(name) {

        super.IsElementPresent(this._searchBtn)
        return super.EnterData(this._clientContactName, name)

    }

    VerifyClientContactAppears() {

        return super.GetTextFromElement(this._resultAppear)

    }

    //SearchByActivities
    EnterActivityName(name) {

        super.IsElementPresent(this._searchBtn)
        return super.EnterData(this._activityName, name)

    }

    VerifyActivityAppears() {

        return super.GetTextFromElement(this._activityName)

    }

    ClickOnExport() {

        return super.ClickOn(this._exportFile)

    }

    //Export Files
    VerifyFileIsDownloaded(file) {

        return super.DownloadFileAndReturnContent(file)

    }

}
export default new Browse()