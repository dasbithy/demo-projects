import Page from "./base.page";


class Opportunity extends Page {

    //-------------------------Locators-------------------------------------------------x    
    get _pageLoad() {
        return $("#container_child td:nth-of-type(2) .button-bold:nth-of-type(1)")
    }
    get _clientCompany() {
        return $("//input[@id='clientName']")
    }
    get _selectClientFromSuggestion() {
        return $("[marginheight] [tabindex] [tabindex='-1']")
    }
    get _existingClientRadioYes() {
        return $("input[value='Yes']")
    }
    get _warningBox() {
        return $("#warningId")
    }
    get _existingClientRadioNo() {
        return $("input[value='No']")
    }
    get _selectBrand() {
        return $("#selectBrandsLink")
    }
    get _multiSelectBox() {
        return $("//select[@id='totalBrands']")
    }
    get _clickBrandFromMultiSelect() {
        return $("//option[contains(text(),'AutoBrand')]")
    }
    get _rightArrow() {
        return $("#brandsMultiselectControl .filt_cell .button:nth-of-type(1)")
    }
    get _clientContact() {
        return $(".clientContactRow .ui-autocomplete-input")
    }
    get _contactFromSuggestion() {
        return $("[marginheight] [tabindex='0']:nth-child(13) [tabindex]")
    }
    get _opportunityName() {
        return $("//input[@name='prospect.brandAssignment']")
    }
    get _oppoType() {
        return $("//select[@id='opportunityTypeDropDown']")
    }
    get _detailedInfo() {
        return $("//div[contains(text(),'DETAILED INFORMATION')]")
    }
    get _originEngagement() {
        return $("input[value='Consultant']")
    }
    get _searchConsultant() {
        return $("//input[@name='prospect.searchConsultant']")
    }
    get _audience() {
        return $("//select[@name='prospect.audience']")
    }
    get _requireCapabilities1() {
        return $("input[value='116']")
    }
    get _requireCapabilities2() {
        return $("input[value='122']")
    }
    get _requireCapabilities3() {
        return $("input[value='130']")
    }
    get _networkPartner() {
        return $("//option[@id='211']")
    }
    get _networkRightArrow() {
        return $("#detailedInformation .filt_cell .button:nth-of-type(1)")
    }
    get _termsOfContact() {
        return $("//input[@name='prospect.termOfContract']")
    }
    get _expectedDecisionDate() {
        return $("//input[@id='assignmentEndDateString']")
    }
    get _estimatedStartRevenue() {
        return $("//input[@id='estimatedStartDateString']")
    }
    get _selectIndustry() {
        return $("//select[@id='clientIndustryId']")
    }
    get _primaryOffice() {
        return $("//select[@id='officeDropDown']")
    }
    get _oppDesc() {
        return $("//textarea[@name='prospect.description']")
    }
    get _annualizedRevenue() {
        return $("//input[@id='potentialRevenue']")
    }
    get _calendarIconStart() {
        return $("//div[@id='assignmentEndDate']//span[@class='glyphicon glyphicon-calendar']")
    }
    get _calendarIconEnd() {
        return $("//div[@id='estimatedStartDate']//span[@class='glyphicon glyphicon-calendar']")
    }
    get _selectDate() {
        return $("//td[contains(text(),'15')]")
    }
    get _selectMonth() {
        return $("//span[contains(text(),'Jun')]")
    }
    get _progressDropdown() {
        return $("//select[@id='status']")
    }
    get _probabilityDropdown() {
        return $("//select[@id='probability']")
    }
    get _currentStageDropdown() {
        return $("//select[@id='currentStage']")
    }
    get _whatHappensNext() {
        return $("//textarea[@name='prospect.nextStep']")
    }
    get _nextStage() {
        return $("//select[@id='nextStage']")
    }
    get _nextKeyDate() {
        return $("#nextKeyDate .glyphicon-calendar")
    }
    get _nextMonthArrow() {
        return $(".datepicker-days .next")
    }
    get _saveButton() {
        return $("//button[@name='save']")
    }
    get _verifyTitle() {
        return $("//font[@class='title']")
    }
    get _editOppoStatus() {
        return $("[title='Change Status'] [aria-hidden]")
    }
    get _editStatusFormPopUp() {
        return $("//form[@id='prospectStatusForm']")
    }
    get _dateAdded() {
        return $("//div[@id='dateAdded']//span[@class='glyphicon glyphicon-calendar']")
    }
    get _dateClosed() {
        return $("//div[@id='dateClosed']//span[@class='glyphicon glyphicon-calendar']")
    }
    get _actualRevenue() {
        return $("//input[@id='actualRevenue']")
    }
    get _statusLevel() {
        return $("#third_stage_label_container .current_stage_label")
    }
    get _lostAndDropped() {
        return $("//select[@id='lostWhen']")
    }
    get _lostAndDroppedItem() {
        return $("//select[@id='lostWhen']//option[contains(text(),'Early Stage, e.g. RFI/RFP/Intro')]")
    }
    get _saveStatusFlyer() {
        return $("//button[@name='updateProspectStatus']//i[@class='fa fa-check']")
    }
    get _onHoldStatus() {
        return $("//div[@class='stage active_stage']")
    }
    get _editOppoAfterSearch() {
        return $("[title='Edit'] [aria-hidden]")
    }
    //-------------------------Locators-------------------------------------------------x 

    Open(path) {
        super.open(path)
    }

    WaitForPageToLoad() {

        return super.IsElementPresent(this._pageLoad)

    }

    EnterClientCompany(text) {

        return super.EnterData(this._clientCompany, text)

    }

    SelectClientCompanyFromSuggestion() {

        super.IsElementPresent(this._selectClientFromSuggestion)

        return super.ClickOn(this._selectClientFromSuggestion)

    }

    SelectExistingClientNo() {
        return super.ClickOn(this._existingClientRadioNo)

    }

    VerifyNoAlertDisplayed() {

        return super.IsElementPresent(this._warningBox)

    }

    SelectExistingClientYes() {

        return super.ClickOn(this._existingClientRadioYes)

    }

    VerifyAlertDisplayed() {

        return super.IsElementPresent(this._warningBox)

    }

    ClickOnSelectBrand() {

        return super.ClickOn(this._selectBrand)

    }

    VerifyMultiSelectOptionDisplayed() {

        return super.IsElementPresent(this._multiSelectBox)

    }

    SelectItemFromMultiSelect() {

        return super.ClickOn(this._clickBrandFromMultiSelect)

    }

    AddtoAssigned() {

        return super.ClickOn(this._rightArrow)

    }

    EnterClientContact(text) {

        return super.EnterData(this._clientContact, text)

    }

    SelectContactFromSuggestion() {

        this.IsElementPresent(this._contactFromSuggestion)
        browser.keys("\uE015")
        return browser.keys("\uE007")

    }

    EnterOpportunityName(text) {

        return super.EnterData(this._opportunityName, text)

    }

    SelectOppoType(type) {

        return super.SelectItem(this._oppoType, type)

    }

    VerifyDetailedInformationAppeared() {

        return super.IsElementPresent(this._detailedInfo)

    }


    SelectOriginOfEngagement() {

        return super.ClickOn(this._originEngagement)

    }

    VerifySearchConsultantFieldAppear() {

        return super.IsElementPresent(this._searchConsultant)

    }

    EnterSearchConsultant(name) {

        return super.EnterData(this._searchConsultant, name)

    }

    SelectAudience(item) {

        return super.SelectItem(this._audience, item)

    }

    SelectRequiredCapabilities() {

        super.ClickOn(this._requireCapabilities1)
        super.ClickOn(this._requireCapabilities2)
        return super.ClickOn(this._requireCapabilities3)

    }

    SelectNetworkPartner() {

        super.ClickOn(this._networkPartner)
        return super.ClickOn(this._networkRightArrow)
    }

    EnterTermsOfContact(count) {

        return super.EnterData(this._termsOfContact, count)

    }

    EnterDecisionDate(date) {

        super.ClickOn(this._calendarIconStart)
        return super.ClickOn($(`//td[contains(text(),'${date}')]`))

    }

    EnterEstimatedDate(month) {

        super.ClickOn(this._calendarIconEnd)
        return super.ClickOn($(`//span[contains(text(),'${month}')]`))

    }


    SelectIndustry() {

        super.ClickOn(this._selectIndustry)
        return super.SelectItemFromDropDown()

    }


    SelectPrimaryOffice() {
        super.IsElementPresent(this._primaryOffice)
        super.ClickOn(this._primaryOffice)
        browser.pause(2000)
        browser.keys("\uE015")
        return browser.keys("\uE007")
    }

    EnterOppoDescription(text) {

        return super.EnterData(this._oppDesc, text)

    }

    EnterPotentialAnnualizedRevenueNumber(count) {

        return super.EnterData(this._annualizedRevenue, count)

    }




    //STATUS
    //In Progress, Won, Lost, Dropped, On Hold
    SelectProgressStatus(status) {

        return super.SelectItem(this._progressDropdown, status)

    }

    //0,10,25,33,50,75,90
    SelectProbability(probability) {

        return super.SelectItem(this._probabilityDropdown, probability)

    }

    //105,106,102,104,101,103,127,131
    SelectCurrentStage() {

        super.ClickOn(this._currentStageDropdown)
        return super.ClickOn($("//select[@id='currentStage']//option[contains(text(),'Decision Stage (Y/N)')]"))

    }

    EnterWhatHappensNext(text) {

        return super.EnterData(this._whatHappensNext, text)

    }

    //105,106,102,104,101,103,127,131
    SelectNextStage() {

        super.ClickOn(this._nextStage)
        return super.ClickOn($("//select[@id='nextStage']//option[contains(text(),'Final Stage, e.g. Final Pitch')]"))

    }

    SelectNextKeyDate(date) {

        super.ClickOn(this._nextKeyDate)
        super.ClickOn(this._nextMonthArrow)
        super.ClickOn(this._nextMonthArrow)
        return super.ClickOn($(`//td[contains(text(),'${date}')]`))


    }

    ClickSaveOpportunity() {

        return super.ClickOn(this._saveButton)

    }

    VerifyOpportunityIsCreated() {

        return super.GetTextFromElement(this._verifyTitle)

    }

    ClickOnEditOpportunityStatus() {
        return super.ClickOn(this._editOppoStatus)

    }

    VerifyStatusPopupIsOpened() {

        return super.IsElementPresent(this._editStatusFormPopUp)

    }


    SelectAddedDate() {

        super.ClickOn(this._dateAdded)
        return super.ClickOn($("//td[@class='today active day']"))

    }

    SelectCloseDate() {

        super.ClickOn(this._dateClosed)
        return super.ClickOn($("//td[@class='today active day']"))

    }

    EnterEstimatedRevenue(amount) {

        return super.EnterData(this._actualRevenue, amount)

    }

    VerifyOpportunityStatus(status) {


        browser.pause(2000)
        return this._statusLevel.getText()
    }

    SelectLostOrDroppedField() {

        super.ClickOn(this._lostAndDropped)
        return super.ClickOn(this._lostAndDroppedItem)

    }

    ClickOnSaveButtonFromPopUp() {

        return super.ClickOn(this._saveStatusFlyer)

    }

    VerifyOnHoldStatus() {

        browser.pause(2000)
        let bgUrl = this._onHoldStatus.getCSSProperty('background-image')
        let bgUrlValue = bgUrl.value
        return bgUrlValue.includes('active_stage.png')
    }


    ClickOnSearchedOppo(oppo) {

        return super.ClickOn($(`//a[contains(text(),'${oppo}')]`))

    }

    ClickOnSearchedOppoEdit() {

        return super.ClickOn(this._editOppoAfterSearch)

    }




}

export default new Opportunity()