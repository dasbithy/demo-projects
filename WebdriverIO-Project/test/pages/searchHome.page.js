import Page from "./base.page";


class SearchItem extends Page {

    //-------------------------Locators-------------------------------------------------x
    get _searchDropdown() {
        return $("//select[@name='searchCategory']")
    }
    get _searchText() {
        return $("//input[@id='quickSearchText']")
    }
    get _goBtn() {
        return $("#quicksearch .button-small")
    }
    get _clientContact() {
        return $("//a[contains(text(),'AutomateMe ClientContact')]")
    }
    get _pageLoad() {
        return $("//button[@class='button button-medium button-bold']")
    }
    //-------------------------Locators-------------------------------------------------x

    WaitForPageToLoad() {
        if (!this._pageLoad.isDisplayed()) {
            this._pageLoad.waitForDisplayed(6000)
        }
    }

    SelectSearchItem(item) {
        browser.pause(3000)
        return this._searchDropdown.selectByAttribute('value', item)
    }

    EnterSearchText(text) {
        return this._searchText.setValue(text)
    }

    ClickOnGoButton() {
        return this._goBtn.click()
    }

    ClickOnClientContact(value) {
        return $(`//a[contains(text(),'${value}')]`).click()
    }



}

export default new SearchItem()