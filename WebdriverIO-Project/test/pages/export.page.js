import Page from "./base.page";
import path from 'path'
const x = path.join(__dirname, '../assets/')
const fs = require('fs')
const checkExistWithTimeout = require("../utils/checkExistTimeout")

class Export extends Page {

    //-------------------------Locators-------------------------------------------------x
    get _verifyPageLoad() {
        return $(".current.next")
    }
    get _createView() {
        return $("[view-shared]")
    }
    get _addView() {
        return $("//a[@id='addNewView']")
    }
    get _viewName() {
        return $("//input[@id='txtViewName']")
    }
    get _createButton() {
        return $("#btnViewSave")
    }
    get _yesFrmDialog() {
        return $(".ui-dialog-buttonset [role='button']:nth-of-type(1) .ui-button-text")
    }
    get _columnLocator() {
        return $("#columnSelector")
    }
    get _selectAllFilter() {
        return $("[field='selectall']")
    }
    get _exportLocator() {
        return $("#export")
    }

    //-------------------------Locators-------------------------------------------------x

    Open(path) {
        super.open(path)
    }

    WaitForPageLoad() {
        browser.pause(8000)
    }

    ClickOnViewMenu() {
        if (!this._createView.isDisplayed()) {
            this._createView.waitForDisplayed(10000)
        }
        return this._createView.click()
    }

    ClickOnAddView() {
        if (!this._addView.isDisplayed()) {
            this._addView.waitForDisplayed(10000)
        }
        return this._addView.click()
    }

    EnterViewName(name) {
        if (!this._viewName.isDisplayed()) {
            this._viewName.waitForDisplayed(6000)
        }
        return this._viewName.setValue(name)
    }

    ClickOnCreateView() {
        browser.pause(4000)
        this._createButton.click()
        browser.waitUntil(() => {
            return $(".introjs-tooltiptext").isDisplayed() === true
        }, 10000)

        if ($(".introjs-tooltiptext").isDisplayed()) {
            $("=Close").click()
        }
    }

    VerifyViewIsCreated() {

        browser.pause(6000)
        return $("#viewSelectorName").getText()

    }

    ClickOnDeleteView(viewName) {
        // if($("//div[@class='introjs-tooltip']").isDisplyed()){
        //     $("//a[@class='introjs-button introjs-skipbutton']").click()
        // }
        browser.pause(8000)

        this.ClickOnViewMenu()
        browser.pause(4000)
        let viewNames = $$(".viewTable tbody tr")

        for (let i = 2; i < viewNames.length; i++) {
            let viewNameLocator = $(`//div[@id='views']//table[@class='viewTable']/tbody/tr[${i}]/td[@class='pointer viewName']`).getText()
            if (viewNameLocator.includes(viewName)) {

                return $(`.viewTable tr:nth-of-type(${i}) [title='Delete'] [aria-hidden]`).click()
            }

        }
    }

    ClickOnYesFromDialog() {
        if (!this._yesFrmDialog.isDisplayed()) {
            this._yesFrmDialog.waitForDisplayed(8000)
        }
        return this._yesFrmDialog.click()
    }

    VerifyViewIsDeleted(viewname) {
        browser.pause(3000)
        // if($("//div[@class='introjs-tooltip']").isDisplyed()){
        //     $("//a[@class='introjs-button introjs-skipbutton']").click()
        // }
        //this.ClickOnViewMenu()
        browser.pause(8000)
        let viewNames = $$(".viewTable tbody tr")

        for (let i = 2; i < viewNames.length; i++) {
            let viewNameLocator = $(`//div[@id='views']//table[@class='viewTable']/tbody/tr[${i}]/td[@class='pointer viewName']`).getText()

            return viewNameLocator.includes(viewname)

        }

    }

    ClickOnColumnSelector() {
        browser.pause(2000)
        return this._columnLocator.click()
    }

    SelectFilter() {
        browser.pause(3000)
        this._selectAllFilter.click()
        browser.pause(8000)
        return $(".selectColumnButton").click()
    }

    VerifyColumFilterIsSet() {
        browser.pause(8000)
        return $$(".tabulator-header [role='columnheader']").length
    }

    // CloseColumnFilter(){
    //     browser.pause(4000)
    //     return $(".selectColumnButton").click()
    // }

    ClickOnExportLink() {
        browser.pause(2000)
        return this._exportLocator.click()
    }

    ClickOnDownloadXLSX() {
        browser.pause(2000)
        return $("#excel").click()
    }

    ClickOnDownloadPDF() {
        browser.pause(2000)
        return $("#pdf").click()
    }

    //Export Files
    VerifyFileIsDownloaded(file) {
        const filePath = path.join(global.downloadDir, file)
        browser.call(function () {
            // call our custom function that checks for the file to exist
            return checkExistWithTimeout(filePath, 60000)
        });

        const fileContents = fs.readFileSync(filePath, 'utf-8')
        return fileContents
    }


}
export default new Export()