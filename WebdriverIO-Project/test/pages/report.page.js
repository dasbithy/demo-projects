import Page from "./base.page";
import path from 'path'
const fs = require('fs')
const checkExistWithTimeout = require("../utils/checkExistTimeout")

class ReportExport extends Page {

    //-------------------------Locators-------------------------------------------------x
    get _winsContent() {
        return $("#container_child .data:nth-of-type(2) td:nth-of-type(1)")
    }
    get _inProgressContent() {
        return $(".reportHeader")
    }
    get _clientCompanyFromTable() {
        return $("#container_child .data:nth-of-type(2) td:nth-of-type(1)")
    }
    get _exportReportButton() {
        return $("//div[@id='container_child']//button[@class='button button-small button-bold']")
    }

    //get _winsContent(){return $("//h3[@class='reportHeader']")}
    get _activityContent() {
        return $("#container_child [width] .reportHeader")
    }
    get _analysisContent() {
        return $("#container_child > .reportHeader:nth-child(7)")
    }
    get _monthlyManagementContent() {
        $("//table[#'search_results_table']//td[@innertext='Collaborator']")
    }
    get _initiationContent() {
        return $("#container_child .header:nth-of-type(1) [width='170px']:nth-of-type(3)")
    }
    get _initiationYoyContent() {
        return $("#container_child .header:nth-of-type(1) [width='260px']:nth-of-type(3)")
    }
    get _consultYoYContent() {
        return $("#container_child .header:nth-of-type(1) [width='260px']:nth-of-type(3)")
    }
    get _pipelineConent() {
        return $("#container_child .data:nth-of-type(2) td:nth-of-type(1)")
    }
    get _winSurveyContent() {
        return $(".header td:nth-child(13)")
    }
    get _lossSurveyContent() {
        return $(".header td:nth-child(9)")
    }

    get _winsReport() {
        return $("//td[contains(text(),'Wins')]")
    }
    get _inProgressReport() {
        return $("[name] td:nth-of-type(2) .reportType:nth-of-type(2)")
    }
    get _activityReport() {
        return $("[name] td:nth-of-type(2) .reportType:nth-of-type(3)")
    }
    get _stageAnalysisReport() {
        return $("[name] td:nth-of-type(2) .reportType:nth-of-type(4)")
    }
    get _monthlyManagementReport() {
        return $("[name] td:nth-of-type(2) .reportType:nth-of-type(5)")
    }
    get _initiationReport() {
        return $("[name] tr:nth-of-type(2) td .reportType:nth-of-type(1)")
    }
    get _initiationYoyReport() {
        return $("[name] tr:nth-of-type(2) td .reportType:nth-of-type(2)")
    }
    get _consultYOYReport() {
        return $("[name] tr:nth-of-type(2) td .reportType:nth-of-type(3)")
    }
    get _pipelineReport() {
        return $("[name] tr:nth-of-type(2) td .reportType:nth-of-type(4)")
    }
    get _winSurvey() {
        return $("[name] tr:nth-of-type(2) td .reportType:nth-of-type(5)")
    }
    get _lossSurvey() {
        return $("[name] td .reportType:nth-of-type(6)")
    }
    //-------------------------Locators-------------------------------------------------x

    
    Open(path) {
        super.open(path)
    }

    WaitForPageToLoad() {

        return super.IsElementPresent($("//div[@id='footercontainer']"))
        // if(!$("//div[@id='footercontainer']").isDisplayed()){
        //     $("//div[@id='footercontainer']").waitForDisplayed(6000)
        // }
    }

    ClickOnLobTab(lob) {

        return super.ClickOn($(`//a[contains(text(),'${lob}')]`))
        // browser.pause(2000)
        // return $(`//a[contains(text(),'${lob}')]`).click()
    }

    ClickOnWinsReport() {
        browser.pause(2000)
        if (!this._winsReport.isSelected()) {
            return super.ClickOn(this._winsReport)
        }
    }

    ClickOnExportThisReport() {

        return super.ClickOn(this._exportReportButton)
        // browser.pause(2000)
        // return this._exportReportButton.click()
    }


    WinContents() {
        browser.pause(2000)
        return this._winsContent.getText().replace(" ", "")
    }

    VerifyReportIsDownloaded(file) {

        return super.DownloadFileAndReturnContent(file)
        // const filePath = path.join(global.downloadDir, file)
        // browser.call(function (){
        //     // call our custom function that checks for the file to exist
        //     return checkExistWithTimeout(filePath, 60000)
        //   });

        // const fileContents = fs.readFileSync(filePath, 'utf-8')
        // return fileContents
    }

    ClickOnProgressReport() {

        return super.ClickOn(this._inProgressReport)
        // browser.pause(2000)
        // return this._inProgressReport.click()
    }

    ProgressContents() {

        return super.GetTextFromElement(this._inProgressContent)
        // browser.pause(2000)
        // return this._inProgressContent.getText()
    }

    ClickOnActivityReport() {

        return super.ClickOn(this._activityReport)

        // browser.pause(2000)
        // return this._activityReport.click()
    }
    ActivityContents() {

        return super.GetTextFromElement(this._activityContent)
        // browser.pause(2000)
        // return this._activityContent.getText()
    }

    ClickOnStageAnalysisReport() {

        return super.ClickOn(this._stageAnalysisReport)
        // browser.pause(2000)
        // return this._stageAnalysisReport.click()
    }

    StageAnalysisContents() {

        return super.GetTextFromElement(this._analysisContent)
        // browser.pause(2000)
        // return this._analysisContent.getText()
    }

    ClickOnPipelineReport() {

        return super.ClickOn(this._pipelineReport)
        // browser.pause(2000)
        // return this._pipelineReport.click()
    }

    PipelineContents() {

        return super.GetTextFromElement(this._pipelineConent)
        // browser.pause(2000)
        // return this._pipelineConent.getText()
    }

    ClickOnMonthlyManagement() {

        return super.ClickOn(this._monthlyManagementReport)
        // browser.pause(2000)
        // return this._monthlyManagementReport.click()
    }

    MonthlyManagementContents() {

        return super.GetTextFromElement(this._monthlyManagementContent)
        // browser.pause(2000)
        // return this._monthlyManagementContent.getText()
    }

    ClickOnInitiation() {

        return super.ClickOn(this._initiationReport)
        // browser.pause(2000)
        // return this._initiationReport.click()
    }

    InitiationContents() {

        return super.GetTextFromElement(this._initiationContent)
        // browser.pause(2000)
        // return this._initiationContent.getText()
    }

    ClickOnInitiationYOY() {

        return super.ClickOn(this._initiationYoyReport)
        // browser.pause(2000)
        // return this._initiationYoyReport.click()
    }

    InitiationYOYContents() {

        return super.GetTextFromElement(this._initiationYoyContent)
        // browser.pause(2000)
        // return this._initiationYoyContent.getText()
    }

    ClickOnConsultYoY() {

        return super.ClickOn(this._consultYOYReport)
        // browser.pause(2000)
        // return this._consultYOYReport.click()
    }

    ConsultYOYContents() {

        return super.GetTextFromElement(this._consultYoYContent)
        // browser.pause(2000)
        // return this._consultYoYContent.getText()
    }

    ClickOnWinSurvey() {

        return super.ClickOn(this._winSurvey)
        // browser.pause(2000)
        // return this._winSurvey.click()
    }

    WinSurveyContents() {

        return super.GetTextFromElement(this._winSurveyContent)
        // browser.pause(2000)
        // return this._winSurveyContent.getText()
    }

    ClickOnLossSurvey() {

        return super.ClickOn(this._lossSurvey)
        // browser.pause(2000)
        // return this._lossSurvey.click()
    }

    LossSurveyContents() {

        return super.GetTextFromElement(this._lossSurveyContent)
        // browser.pause(2000)
        // return this._lossSurveyContent.getText()
    }








}

export default new ReportExport()