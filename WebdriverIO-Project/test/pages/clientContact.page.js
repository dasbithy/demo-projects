import Page from "./base.page";

class ClientContact extends Page {

    //-------------------------Locators-------------------------------------------------x
    get _clientName() {
        return $("//input[@id='clientName']")
    }
    get _ccNameFromSuggestion() {
        return $(".ui-menu .ui-menu-item:nth-child(1)")
    }
    get _waitForIt() {
        return $("//div[contains(text(),'SHARE THIS CLIENT CONTACT')]")
    }
    get _firstName() {
        return $("//input[@name='contact.name']")
    }
    get _lastName() {
        return $("//input[@name='contact.surname']")
    }
    get _enterEmail() {
        return $("//input[@name='contact.email']")
    }
    get _saveBtn() {
        return $("//button[@name='create']")
    }
    get _cContactTitle() {
        return $(".title")
    }
    get _radioShareLobLevel() {
        return $("//input[@id='shareAtIndividualOrOfficeLevel']")
    }
    get _userNameLobShare() {
        return $("//input[@id='username']")
    }
    get _chooseFile() {
        return $("//input[@name='contactFileBean']")
    }
    get _uploadFile() {
        return $("//input[@id='uploadContactFileButton']")
    }
    get _uploadedFile() {
        return $("//td[@class='file_name_cell bottombufferborder first_file_cell']")
    }
    get _downloadLink() {
        return $("//table[@id='contactFiles']//a[@title='Download']")
    }
    get _deleteFile() {
        return $(".deleteContactFile [aria-hidden]")
    }
    //-------------------------Locators-------------------------------------------------x
    
    Open(path) {
        super.open(path)
    }

    WaitForPageToLoad() {
        return super.WaitForElementToBeLoaded(this._waitForIt)
    }

    EnterClientCompanyFromSuggestions(ccname) {
        super.EnterData(this._clientName, ccname)
        return this._ccNameFromSuggestion.click()

    }

    EnterFirstName(firstname) {
        return super.EnterData(this._firstName, firstname)
    }

    EnterLastName(lastname) {
        return super.EnterData(this._lastName, lastname)
    }

    EnterEmail(email) {
        return super.EnterData(this._enterEmail, email)
    }

    ClickOnSave() {
        return super.ClickOn(this._saveBtn)
    }

    VerifyClientContactIsCreated() {
        return super.GetTextFromElement(this._cContactTitle)
    }

    ClickOnShareLobLevel() {
        return super.ClickOn(this._radioShareLobLevel)
    }

    VerifyShareWithIndividualAppears() {
        return super.IsElementPresent(this._userNameLobShare)
    }

    EnterAndSelectUserName(name) {
        super.EnterData(this._userNameLobShare, name)
        browser.pause(5000)
        browser.keys("\uE015")
        browser.keys("\uE015")
        return browser.keys("\uE006")
    }

    ChooseFile(file) {
        return super.UploadAsset(this._chooseFile, file)
    }

    ClickOnUploadFile() {
        return super.ClickOn(this._uploadFile)
    }

    VerifyFileIsUploaded() {
        return super.IsElementPresent(this._uploadedFile)
    }

    ClickOnDownloadFile() {
        return super.ClickOn(this._downloadLink)
    }

    VerifyFileIsDownloaded(file) {
        return super.DownloadFileAndReturnContent(file)
    }

    ClickOnDeleteFile() {
        return super.ClickOn(this._deleteFile)
    }

    VerifyFileIsDeleted() {
        return super.IsElementPresent(this._uploadedFile)
    }

}

export default new ClientContact()