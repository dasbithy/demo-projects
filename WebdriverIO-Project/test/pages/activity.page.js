import Page from "./base.page";


class Activity extends Page {

    //-------------------------Locators-------------------------------------------------x
    get _activityTitle() {
        return $("//input[@name='activity.title']")
    }
    get _saveBtn() {
        return $("//button[@name='create']")
    }
    get _lobId() {
        return $("//select[@id='lobId']")
    }
    get _activityType() {
        return $("//select[@id='activityType']")
    }
    get _calendarIconStart() {
        return $("#activityFromDate .glyphicon-calendar")
    }
    get _calendarIconEnd() {
        return $("#activityEndDate .glyphicon-calendar")
    }
    get _startDate() {
        return $("//input[@name='activity.fromDateString']")
    }
    get _endDate() {
        return $("//input[@name='activity.toDateString']")
    }
    get _focusOut() {
        return $("//html//body")
    }
    get _descriptionTtext() {
        return $("[cols]")
    }
    get _verifyTitle() {
        return $(".title")
    }
    //-------------------------Locators-------------------------------------------------x

    Open(path) {
        super.open(path)
    }

    WaitForPageToLoad() {
        super.WaitForElementToBeLoaded(this._saveBtn)
    }

    EnterActivityTitle(title) {
        return super.EnterData(this._activityTitle, title)
    }

    SelectLob(id) {
        return super.SelectItem(this._lobId, id)
    }

    SelectActivityType(type) {
        return super.SelectItem(this._activityType, type)
    }


    SelectActivityFrom(startDate) {
        return super.EnterData(this._startDate, startDate)
    }


    SelectActivityTo(endDate) {
        return super.EnterData(this._endDate, endDate)
    }

    EnterDescription(text) {
        super.ClickOn(this._focusOut)
        return super.EnterData(this._descriptionTtext, text)
    }

    ClickOnSave() {
        return super.ClickOn(this._saveBtn)
    }

    VerifyActivityIsCreated() {
        return super.GetTextFromElement(this._verifyTitle)
    }




}

export default new Activity()