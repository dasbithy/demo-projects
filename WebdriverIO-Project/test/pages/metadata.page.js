import Page from "./base.page";


class Metadata extends Page {

    //-------------------------Locators-------------------------------------------------x
    get _addBtn() {
        return $("#addFieldImage")
    }
    get _addFieldDialog() {
        return $("#addFieldDialog")
    }
    get _nameField() {
        return $("//input[@id='addFieldName']")
    }
    get _fieldType() {
        return $("//select[@id='addFieldType']")
    }
    get _requiredCheckbox() {
        return $("#addFieldRequiredCheckBox")
    }
    get _clickOnSave() {
        return $("#addFieldDialog .button-medium")
    }
    get _confirmDelete() {
        return $("[marginheight] [tabindex='-1']:nth-child(11) .ui-dialog-buttonset [role='button']:nth-of-type(1)")
    }
    //-------------------------Locators-------------------------------------------------x


    Open(path) {
        super.open(path)
    }

    WaitForPageToLoad() {
        browser.pause(4000)
    }

    ClickOnLob(lob) {
        browser.pause(2000)
        return $(`=${lob}`).click()
    }

    ClickOnAddMetadata() {
        browser.pause(3000)
        return this._addBtn.click()
    }

    VerifyAddFieldDialogAppears() {
        if (!this._addFieldDialog.isDisplayed()) {
            this._addFieldDialog.waitForDisplayed(6000)
        }
        return this._addFieldDialog.isDisplayed()
    }

    EnterFieldName(name) {
        browser.pause(2000)
        return this._nameField.setValue(name)
    }

    SelectFieldType(type) {
        browser.pause(2000)
        return this._fieldType.selectByAttribute('value', type)
    }

    IsItRequiredCheckBox(check) {
        browser.pause(2000)
        if (check) {
            return this._requiredCheckbox.click()
        }
    }

    ClickOnSave() {
        browser.pause(2000)
        return this._clickOnSave.click()
    }

    VerifyMetaDataIscreated(name) {
        browser.pause(2000)
        browser.refresh()
        browser.pause(4000)
        return $(`//td[contains(text(),'${name}')]`).isDisplayed()
    }


    DeleteMetadata(name) {
        browser.pause(2000)
        browser.refresh()
        browser.pause(4000)
        let fieldName = name
        let rowCount = $$(".sorting_1")
        for (let count = 1; count <= rowCount.length; count++) {

            if (count % 2 == 0) {
                let evenRow = $(`[role] .even:nth-of-type(${count}) .sorting_1`).getText()
                if (evenRow.includes(fieldName)) {
                    return $(`.even:nth-of-type(${count}) td:nth-of-type(1) .manage_metadata_link:nth-of-type(2)`).click()
                }
            } else {
                let oddRow = $(`[role] .odd:nth-of-type(${count}) .sorting_1`).getText()
                if (oddRow.includes(fieldName)) {
                    return $(`.odd:nth-of-type(${count}) td:nth-of-type(1) .manage_metadata_link:nth-of-type(2)`).click()
                }

            }
        }
    }

    ConfirmDelete() {
        browser.pause(4000)
        return this._confirmDelete.click()

    }

    VerifyMetaDataIsDeleted(name) {
        browser.pause(2000)
        browser.refresh()
        browser.pause(4000)
        return $(`//td[contains(text(),'${name}')]`).isDisplayed()
    }

    VerifyMetadataAppearsInOppoPage(metadata) {
        browser.pause(6000)
        return $(`//b[contains(text(),'${metadata}:')]`).isDisplayed()
    }


    ClickOnEditOppo() {
        browser.pause(3000)
        return $("//div[@id='container_child']/table[1]//td/a[@title='Edit']").click()
    }



}

export default new Metadata()