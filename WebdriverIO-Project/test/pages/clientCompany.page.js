import Page from "./base.page";
import path from 'path'
const x = path.join(__dirname, '../assets/')
const fs = require('fs')
const checkExistWithTimeout = require("../utils/checkExistTimeout")
class ClientCompany extends Page {

    //-------------------------Locators-------------------------------------------------x
    get _clientCompanyLink() {
        return $("=Client Company")
    }
    get _clientCompanyName() {
        return $("//input[@id='name']")
    }
    get _saveBtn() {
        return $("//button[@name='create']")
    }
    get _lobId() {
        return $("//select[@id='lobId']")
    }
    get _relationDropdown() {
        return $("//select[@name='client.relation']")
    }
    get _industrySector() {
        return $("//select[@name='client.industryid']")
    }
    get _saveBtn() {
        return $("//button[@name='create']")
    }
    get _ccTitle() {
        return $(".title")
    }
    get _addNote() {
        return $("//button[contains(text(),'Add Note')]")
    }
    get _noteTextArea() {
        return $("//textarea[@id='fmNoteValue']")
    }
    get _noteSaveBtn() {
        return $("//input[@id='fmNoteSave']")
    }
    get _notePrivate() {
        return $("//input[@id='fmNotePvt']")
    }
    get _addedNote() {
        return $("//div[contains(text(),'Automation is going on')]")
    }
    get _privateAnnotate() {
        return $("//span[@class='red']")
    }
    get _editNote() {
        return $("//span[contains(text(),'Edit')]")
    }
    get _deleteNote() {
        return $("//a[@class='deleteNote']")
    }
    get _noteDeleted() {
        return $("//div[@class='note']")
    }
    //get _alertOkBtn(){return $("//div[5]/div[3]/div/button[1]/span[@innertext='Ok']")}
    get _alertOkBtn() {
        return $("div.ui-dialog.ui-widget.ui-widget-content.ui-corner-all.ui-front.ui-dialog-buttons:nth-child(7) div.ui-dialog-buttonpane.ui-widget-content.ui-helper-clearfix div.ui-dialog-buttonset button.ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-text-only:nth-child(1) > span.ui-button-text")
    }
    get _chooseFile() {
        return $("//input[@name='clientFileBean']")
    }
    get _uploadBtn() {
        return $("//input[@id='uploadClientFileButton']")
    }
    get _uploadedFile() {
        return $("//td[@class='file_name_cell first_file_cell']")
    }
    get _deleteFile() {
        return $("//a[@class='deleteClientFile']")
    }
    get _downloadLink() {
        return $("//table[@id='clientFiles']//a[@title='Download']")
    }
    get _editClientCompany() {
        return $("//a[@title='Edit']")
    }
    get _brandText() {
        return $("#brandsText")
    }
    get _addBrandButton() {
        return $("#addBrandButton")
    }
    get _brandIsAdded() {
        return $(".brand_label_div")
    }
    get _editBrand() {
        return $("//i[@class='fa fa-pencil-square-o font-size-20 editBrandImage']")
    }
    get _brandInput() {
        return $(".brand_input")
    }
    get _deletBrand() {
        return $("//i[@class='fa fa-trash font-size-20 deleteBrandImage']")
    }
    get _updateBtn() {
        return $("//button[@name='update']")
    }
    get _verifyBrand() {
        return $(".view_table td:nth-of-type(1) [bgcolor='\#FFFFFF']:nth-of-type(1) .options_column")
    }
    get _brandDeleteAlert() {
        return $("[marginheight] [tabindex='-1']:nth-child(10) div:nth-of-type(3) [role='button']:nth-of-type(1) .ui-button-text")
    }
    get _clientCompanyEditBtn() {
        return $("//div[@id='container_child']/table[1]//td/a[@title='Edit']")
    }
    //-------------------------Locators-------------------------------------------------x

    Open(path) {
        super.open(path)
    }

    WaitForPageToLoad() {
        // if (!this._saveBtn.isDisplayed()) {
        //     this._saveBtn.waitForDisplayed(4000)
        // }

        return super.WaitForElementToBeLoaded(this._saveBtn)
    }

    EnterClientCompanyName(title) {
        //return this._clientCompanyName.setValue(title)
        return super.EnterData(this._clientCompanyName, title)
    }

    SelectLob(value) {

        return super.SelectItem(this._lobId, value)
        // browser.pause(2000)
        // return this._lobId.selectByAttribute('value', value)
    }

    SelectRelation(value) {
        return super.SelectItem(this._relationDropdown, value)
        // browser.pause(2000)
        // return this._relationDropdown.selectByAttribute('value', value)
    }

    SelectIndustrySector() {

        super.ClickOn(this._industrySector)
        // browser.pause(5000)
        // this._industrySector.click()
        return super.SelectItemFromDropDown()
    }

    ClickOnSaveButton() {
        return super.ClickOn(this._saveBtn)
        //return this._saveBtn.click()
    }

    VerifyClientCompanyIsCreated() {
        return super.GetTextFromElement(this._ccTitle)
        //return this._ccTitle.getText()
    }

    ClickOnAddNote() {
        return super.ClickOn(this._addNote)
        //return this._addNote.click()
    }

    EnterNotes(text) {

        super.IsElementExist(this._noteTextArea)
        // // super.IsElementPresent(this._noteTextArea)
        // // return super.EnterData(this._noteTextArea, text)
        // if (!this._noteTextArea.isDisplayed()) {
        //     this._noteTextArea.waitForDisplayed(6000)
        // }
        return this._noteTextArea.setValue(text)
    }

    ClickSaveNote() {
        return super.ClickOn(this._noteSaveBtn)
        //return this._noteSaveBtn.click()
    }

    VerifyNotesAdded() {
        // return super.GetTextFromElement(this._addedNote)
        return this._addedNote.getText()
    }

    ClickOnEditButton() {

        super.IsElementPresent(this._editNote)
        return super.ClickOn(this._editNote)
        // if (!this._editNote.isDisplayed) {
        //     this._editNote.waitForDisplayed(5000)
        // }
        // return this._editNote.click()
    }

    EnterNotesToEdit(text) {
        return super.EnterData(this._noteTextArea, text)
        //return this._noteTextArea.setValue(text)
    }

    MakeNotePrivate() {
        return super.ClickOn(this._notePrivate)
        //return this._notePrivate.click()
    }

    VerifyNoteIsPrivate() {
        return super.IsElementPresent(this._privateAnnotate)
        //return this._privateAnnotate.isDisplayed()
    }

    ClickOnDelete() {
        return super.ClickOn(this._deleteNote)
        //return this._deleteNote.click()
    }

    ClickOnOkButton() {
        return super.ClickOn(this._alertOkBtn)
        // browser.pause(4000)
        // return this._alertOkBtn.click()
    }

    VerifyNoteIsDeleted() {
        return super.IsElementPresent(this._noteDeleted)
        // browser.pause(5000)
        // return this._noteDeleted.isDisplayed()
    }

    ChooseFile(file) {

        return super.UploadAsset(this._chooseFile, file)
        //return this._chooseFile.setValue(x + file)
    }

    ClickOnUpload() {
        return super.ClickOn(this._uploadBtn)
        //return this._uploadBtn.click()
    }

    VerifyFileIsUploaded() {

        return super.IsElementPresent(this._uploadedFile)
        // if (!this._uploadedFile.isDisplayed()) {
        //     this._uploadedFile.waitForDisplayed(3000)
        // }
        // return this._uploadedFile.isDisplayed()
    }

    ClickOnDeleteFile() {

        return super.ClickOn(this._deleteFile)
        // if (!this._deleteFile.isDisplayed()) {
        //     this._deleteFile.waitForDisplayed(6000)
        // }
        // return this._deleteFile.click()
    }

    VerifyFileIsDeleted() {
        return super.IsElementPresent(this._uploadedFile)
        // browser.pause(3000)
        // return this._uploadedFile.isDisplayed()
    }

    ClickOnDownloadButtn() {
        return super.ClickOn(this._downloadLink)
        // browser.pause(3000)
        // return this._downloadLink.click()
    }

    VerifyFileIsDownloaded(file) {

        return super.DownloadFileAndReturnContent(file)
        // const filePath = path.join(global.downloadDir, "testCompany.txt")
        // browser.call(function () {
        //     // call our custom function that checks for the file to exist
        //     return checkExistWithTimeout(filePath, 60000)
        // });

        // const fileContents = fs.readFileSync(filePath, 'utf-8')
        // return fileContents
    }


    ClickOnEditIcon() {
        return super.ClickOn(this._editClientCompany)
        //return this._editClientCompany.click()
    }

    EnterBrandText(text) {
        return super.EnterData(this._brandText, text)
        // browser.pause(3000)
        // return this._brandText.setValue(text)
    }

    ClickOnAddBrandBtn() {
        return super.ClickOn(this._addBrandButton)
        //return this._addBrandButton.click()
    }

    VerifyBrandIsAdded() {
        return super.GetTextFromElement(this._brandIsAdded)
        //return this._brandIsAdded.getText()
    }

    ClickOnEditBrand() {
        return super.ClickOn(this._editBrand)
        // if (!this._editBrand.isDisplayed()) {
        //     this._editBrand.waitForDisplayed(6000)
        // }
        // return this._editBrand.click()
    }

    EnterBrandToEdit(text) {
        super.EnterData(this._brandInput, text)
        return super.ClickOn(this._addBrandButton)
        // this._brandInput.setValue(text)
        // return this._addBrandButton.click()
    }

    DeleteBrand() {

        return super.ClickOn(this._deletBrand)
        // browser.pause(3000)
        // return this._deletBrand.click()
    }

    VerifyBrandIsDeleted() {

        return super.IsElementPresent(this._deletBrand)
        //return this._deletBrand.isDisplayed()
    }

    ClickOnUpdate() {

        return super.ClickOn(this._updateBtn)
        //return this._updateBtn.click()
    }

    VerifyClientCompanyIsUpdated() {

        return super.GetTextFromElement(this._verifyBrand)
        // browser.pause(3000)
        // return this._verifyBrand.getText()
    }

    ClickOnBrandDeleteAlertOk() {

        return super.ClickOn(this._brandDeleteAlert)
        //return this._brandDeleteAlert.click()
    }

    ClickOnEditClientCompanyIcon() {

        return super.ClickOn(this._clientCompanyEditBtn)
        // browser.pause(3000)
        // return this._clientCompanyEditBtn.click()
    }



}

export default new ClientCompany()