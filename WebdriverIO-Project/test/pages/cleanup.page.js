import Page from "./base.page";

class CleanUp extends Page {

    //-------------------------Locators-------------------------------------------------x
    get _deleteOppo() {
        return $("//button[@name='moveToHistory']")
    }
    get _deleteClient() {
        return $("//button[@name='delete']")
    }
    //-------------------------Locators-------------------------------------------------x

    Open(path) {
        super.open(path)
    }

    ClickOnDeleteButton() {
        if (!this._deleteOppo.isDisplayed()) {
            this._deleteOppo.waitForDisplayed(10000)
        }
        return this._deleteOppo.click()
    }

    VerifyOppoIsDeleted(oppo) {
        browser.pause(2000)
        return $(`//a[contains(text(),'${oppo}')]`).isDisplayed()
    }

    ClickOnDeleteClientCompany() {
        if (!this._deleteClient.isDisplayed()) {
            this._deleteClient.waitForDisplayed(10000)
        }
        this._deleteClient.click()
        browser.acceptAlert()
    }

    VerifyClientCompanyIsDeleted(client) {
        browser.pause(2000)
        return $(`//a[contains(text(),'${client}')]`).isDisplayed()
    }
}

export default new CleanUp()