import Page from "./base.page";


class UserAdmin extends Page {

    //-------------------------Locators-------------------------------------------------x
    get _manageLink() {
        return $("//a[@id='manageId']")
    }
    get _verifyPageLoad() {
        return $("//button[@name='updateUserSubscription']//i[@class='fa fa-check']")
    }
    get _userAdminLink() {
        return $("//a[contains(text(),'User Admin')]")
    }
    get _userAdminPageLoad() {
        return $("[name] div:nth-child(9) .btn-gray")
    }
    get _addUserToBusiness() {
        return $("#addUser")
    }
    get _verifyModalAppear() {
        return $("//div[@id='addUserDialog']")
    }
    get _directoryTypeUser() {
        return $(".blacktxt:nth-of-type(2) [type='radio']:nth-of-type(1)")
    }
    get _nonDirectoryTypeUser() {
        return $(".blacktxt:nth-of-type(2) [type='radio']:nth-of-type(2)")
    }
    get _userName() {
        return $("//input[@id='userName']")
    }
    get _addUserBtn() {
        return $("//button[@id='addUserButton']")
    }
    get _addNonDirectoryUserBtn() {
        return $("[name='addNonDirectoryUser']")
    }
    get _successDialog() {
        return $("//div[@id='dialogSuccess']")
    }
    get _okButton() {
        return $("[marginheight] [tabindex='-1']:nth-child(9) div:nth-of-type(3) .ui-button-text")
    }
    get _saveBtnForChanges() {
        return $("[name] div:nth-child(9) [class='button button-medium button-bold margin-right-10']")
    }
    get _selectOffice() {
        return $("#userOfficeSelect")
    }
    get _enterUpn() {
        return $(".non-directory-user-field-row:nth-of-type(4) [type]")
    }
    get _filterInput() {
        return $("//input[@id='tableFilter']")
    }
    get _flagInactiveUser() {
        return $("//nobr[contains(text(),'(Flag Inactive Users)')]")
    }
    get _statusUser() {
        return $("//th[contains(text(),'Status in directory')]")
    }
    get _downloadLinkUserAdmin() {
        return $("[name] div:nth-child(3) .btn-gray")
    }
    get _savePermission() {
        return $("[name='saveUserPermissions']")
    }
    //-------------------------Locators-------------------------------------------------x
    
    Open(path) {
        super.open(path)
    }

    WaitForPageToLoad() {

        return super.IsElementPresent(this._verifyPageLoad)
    }

    ClickOnUserAdminTab() {
        return super.ClickOn(this._userAdminLink)
    }

    WaitForUserAdminPageToLoad() {

        return super.IsElementPresent(this._userAdminPageLoad)
    }

    ClickOnAddNewUserToNewBusinessGroup() {
        return super.ClickOn(this._addUserToBusiness)
    }

    VerifyModalAppear() {
        return super.IsElementPresent(this._verifyModalAppear)
    }

    ClickOnDirectoryType() {

        return super.ClickOn(this._directoryTypeUser)

    }

    EnterNametoSelectUser(name) {

        super.EnterData(this._userName, name)
        browser.pause(4000)
        browser.keys("\uE015")
        return browser.keys("\uE007")
    }

    ClickOnAdd() {

        return super.ClickOn(this._addUserBtn)
    }

    VerifyUserSuccessDialogIsDisplayed() {

        return super.IsElementPresent(this._successDialog)
    }

    ClickOnOkFromDialog() {

        browser.pause(2000)
        return this._okButton.click()
    }

    VerifyDirectoryUserIsAdded(email) {

        return super.IsElementPresent($(`//a[contains(text(),'${email}')]`))

    }



    ClickOnSaveButton() {

        return super.ClickOn(this._saveBtnForChanges)
    }

    ClickOnDelete(searchText) {
        browser.pause(3000)
        let lookForText = searchText
        let rowCount = $$("#usersTable tr")
        for (let count = 1; count <= rowCount.length; count++) {

            if (count % 2 == 0) {
                let evenRow = $(`#usersTable [onmouseover='ChangeBackgroundColor\(this\)']:nth-of-type(${count}) .evenRow:nth-of-type(3)`).getText()
                if (evenRow.includes(lookForText)) {
                    return super.ClickOn($(`//table[@id='usersTable']/tbody/tr[${count}]/td[@title='Delete']/input[@name='usersArray']`))
                }
            } else {
                let oddRow = $(`#usersTable [onmouseover='ChangeBackgroundColor\(this\)']:nth-of-type(${count}) .oddRow:nth-of-type(2)>a`).getText()
                if (oddRow.includes(lookForText)) {
                    return super.ClickOn($(`//table[@id='usersTable']/tbody/tr[${count}]/td[@title='Delete']/input[@name='usersArray']`))
                }
            }
        }
    }

    ClickOnEditButton(searchText) {
        browser.pause(3000)
        let lookForText = searchText
        let rowCount = $$("#usersTable tr")
        for (let count = 1; count <= rowCount.length; count++) {

            if (count % 2 == 0) {
                let evenRow = $(`#usersTable [onmouseover='ChangeBackgroundColor\(this\)']:nth-of-type(${count}) .evenRow:nth-of-type(3)`).getText()
                if (evenRow.includes(lookForText)) {
                    return super.ClickOn($(`#usersTable [onmouseover='ChangeBackgroundColor\(this\)']:nth-of-type(${count}) [aria-hidden]`))
                }
            } else {
                let oddRow = $(`#usersTable [onmouseover='ChangeBackgroundColor\(this\)']:nth-of-type(${count}) .oddRow:nth-of-type(2)>a`).getText()
                if (oddRow.includes(lookForText)) {
                    return super.ClickOn($(`#usersTable [onmouseover='ChangeBackgroundColor\(this\)']:nth-of-type(${count}) [aria-hidden]`))
                }
            }
        }
    }

    WaitForPermissionPopUpToLoad() {
        if (!this._savePermission.isDisplayed()) {
            browser.waitUntil(() => {
                return this._savePermission.isDisplayed() === true;
            }, 30000);
        }
    }

    ClickOnDeleteToDeleteDirectoryUser(email) {
        browser.pause(3000)
        let emailAddress = email
        let rowCount = $$("#usersTable tr")
        for (let count = 1; count <= rowCount.length; count++) {

            if (count % 2 == 0) {
                let evenRow = $(`#usersTable [onmouseover='ChangeBackgroundColor\(this\)']:nth-of-type(${count}) .evenRow:nth-of-type(3)`).getText()
                if (evenRow.includes(emailAddress)) {
                    return super.ClickOn($(`//table[@id='usersTable']/tbody/tr[${count}]/td[@title='Delete']/input[@name='usersArray']`))
                }
            } else {
                let oddRow = $(`#usersTable .oddRow:nth-of-type(${count}) [data-type]`).getText()
                if (oddRow.includes(emailAddress)) {
                    return super.ClickOn($(`//table[@id='usersTable']/tbody/tr[${count}]/td[@title='Delete']/input[@name='usersArray']`))
                }
            }
        }

    }


    ClickOnNonDirectoryType() {

        return super.ClickOn(this._nonDirectoryTypeUser)
    }

    SelectOffice() {

        super.ClickOn(this._selectOffice)
        browser.pause(2000)
        browser.keys("\uE015")
        return browser.keys("\uE007")
    }

    EnterUpn(email) {

        return super.EnterData(this._enterUpn, email)
    }

    ClickOnAddNonDirectoryUser() {

        return super.ClickOn(this._addNonDirectoryUserBtn)
    }

    VerifyNonDirectoryUserIsAdded(email) {

        return super.IsElementPresent($(`//a[contains(text(),'${email}')]`))
    }

    VerifyAddedUserIsDeleted(email) {

        return super.IsElementPresent($(`//a[contains(text(),'${email}')]`))
    }

    EnterFilterText(text) {

        return super.EnterData(this._filterInput, text)
    }

    VerifyFilterUserIsDisplayed(email) {
        return super.IsElementPresent($(`//a[contains(text(),'${email}')]`))
    }

    ClickOnFlagInactiveUserLink() {

        return super.ClickOn(this._flagInactiveUser)
    }

    VerifyStatusInDirectoryColumnAppears() {

        return super.IsElementPresent(this._statusUser)
    }

    ClickOnDownloadLink() {

        return super.ClickOn(this._downloadLinkUserAdmin)
    }

    SelectPermission() {
        super.ClickOn($("#dialogPermissions table:nth-child(2) [title='Y\&R Admin'] .chk"))
        super.ClickOn($("#dialogPermissions [title='Wunderman Reporter']:nth-of-type(2) .chk"))
        return super.ClickOn(this._savePermission)
    }

    VerifyPermissionIsAdded(searchText) {
        browser.pause(3000)
        let lookForText = searchText
        let rowCount = $$("#usersTable tr")
        for (let count = 1; count <= rowCount.length; count++) {

            if (count % 2 == 0) {
                let evenRow = $(`#usersTable [onmouseover='ChangeBackgroundColor\(this\)']:nth-of-type(${count}) .evenRow:nth-of-type(3)`).getText()
                if (evenRow.includes(lookForText)) {
                    return super.IsElementPresent($(`#usersTable [onmouseover='ChangeBackgroundColor\(this\)']:nth-of-type(${count}) [onerror]`))
                }
            } else {
                let oddRow = $(`#usersTable [onmouseover='ChangeBackgroundColor\(this\)']:nth-of-type(${count}) .oddRow:nth-of-type(2)>a`).getText()
                if (oddRow.includes(lookForText)) {
                    return super.IsElementPresent($(`#usersTable [onmouseover='ChangeBackgroundColor\(this\)']:nth-of-type(${count}) [onerror]`))
                }
            }
        }
    }


}

export default new UserAdmin()