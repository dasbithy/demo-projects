import Page from "./base.page";
import path from 'path'
const x = path.join(__dirname, '../assets/')
const fs = require('fs')
const checkExistWithTimeout = require("../utils/checkExistTimeout")

class ValidateRevenue extends Page {

    //-------------------------Locators-------------------------------------------------x
    get _revenueTab() {
        return $("[href='\/user\/ValidateRevenue\.action']")
    }
    get _exportThisReport() {
        return $(".bottom-verticle")
    }
    get _itemFromTable() {
        return $(".header td:nth-of-type(1)")
    }
    //-------------------------Locators-------------------------------------------------x
    
    Open(path) {
        super.open(path)
    }

    WaitForPageLoad() {
        browser.pause(3000)
    }

    ClickOnValidateRevenue() {
        browser.pause(2000)
        return this._revenueTab.click()
    }

    VerifyValidateRevenuePage() {
        browser.pause(2000)
        return this._exportThisReport.isDisplayed()
    }

    ClickOnExportButton() {
        browser.pause(2000)
        return this._exportThisReport.click()
    }

    GetItemFromTable() {
        browser.pause(2000)
        return this._itemFromTable.getText()
    }

    //Export Files
    VerifyFileIsDownloaded(file) {
        const filePath = path.join(global.downloadDir, file)
        browser.call(function () {
            // call our custom function that checks for the file to exist
            return checkExistWithTimeout(filePath, 60000)
        });

        const fileContents = fs.readFileSync(filePath, 'utf-8')
        return fileContents
    }



}

export default new ValidateRevenue()