import Page from "./base.page";

class OfficeAdmin extends Page {

    //-------------------------Locators-------------------------------------------------x    
    get _officeAdminLink() {
        return $("//a[contains(text(),'Office Admin')]")
    }
    get _pageLoad() {
        return $("//div[@id='footerwrap']")
    }
    get _addOfficeBtn() {
        return $("#addOffice")
    }
    get _verifyOfficeForm() {
        return $("#addOfficeForm")
    }
    get _directoryTypeOffice() {
        return $(".blacktxt:nth-of-type(2) [type='radio']:nth-of-type(1)")
    }
    get _nonDirectoryTypeOffice() {
        return $(".blacktxt:nth-of-type(2) [type='radio']:nth-of-type(2)")
    }
    get _officeName() {
        return $("#officeName")
    }
    get _addOfficeBtnLink() {
        return $("[name='addDirectoryOffice']")
    }
    get _addNonDirectoryOfficeBtn() {
        return $("[name='addNonDirectoryOffice']")
    }
    get _successDialog() {
        return $("//div[@id='dialogSuccess']")
    }
    get _okButtonErrorDialog() {
        return $("[marginheight] [tabindex='-1']:nth-child(7) div:nth-of-type(3) .ui-button-text")
    }
    get _dialogError() {
        return $("#dialogError")
    }
    get _officeLobItem() {
        return $("#officeLobSelect")
    }
    get _companyInput() {
        return $(".non-directory-office-field-row:nth-of-type(4) [type]")
    }
    get _regionItem() {
        return $("#officeRegionSelect")
    }
    get _contryItem() {
        return $("#officeCountrySelect")
    }
    get _cityItem() {
        return $(".non-directory-office-field-row:nth-of-type(7) [type]")
    }
    get _okButtnFromSuccessDialog() {
        return $("[marginheight] [tabindex='-1']:nth-child(8) div:nth-of-type(3) .ui-button-text")
    }
    get _sortByOfficeType() {
        return $("//select[@id='officeTypeSelect']")
    }
    get _filterOffice() {
        return $("//input[@id='officeFilter']")
    }
    get _syncOffice() {
        return $("//table[@id='officeDataTable']/tbody/tr[1]//a[@href='javascript:void(0)']")
    }
    get _syncVerifyMessage() {
        return $("#dialogSuccess")
    }
    get _syncOkButtn() {
        return $("[marginheight] [tabindex='-1']:nth-child(8) div:nth-of-type(3) .ui-button-text")
    }
    //-------------------------Locators-------------------------------------------------x

    Open(path) {
        super.open(path)
    }

    ClickOnOfficeAdminTab() {
        return super.ClickOn(this._officeAdminLink)
    }

    WaitForPageToLoad() {
        return super.IsElementPresent(this._pageLoad)
    }

    ClickOnAddOffice() {
        return super.ClickOn(this._addOfficeBtn)
    }

    VerifyOfficeFormAppears() {
        return super.IsElementPresent(this._verifyOfficeForm)

    }

    SelectDirectoryAsOfficeType() {
        return super.ClickOn(this._directoryTypeOffice)
    }

    EnterOfficeName(name) {
        super.EnterData(this._officeName, name)
        browser.pause(4000)
        browser.keys("\uE015")
        browser.keys("\uE015")
        return browser.keys("\uE007")
    }

    ClickOnAddBtn() {
        return super.ClickOn(this._addOfficeBtnLink)
    }

    VerifyOfficeIsAdded() {
        browser.pause(3000)
        let successText = this._successDialog.getText()
        if (this._dialogError.isDisplayed() || successText.includes("Successfully")) {
            this._okButtonErrorDialog.click()
            return true
        }
    }

    ClickOnNonDirectoryType() {
        return super.ClickOn(this._nonDirectoryTypeOffice)
    }

    EnterNonDirectoryName(text) {
        return super.EnterData(this._officeAdminLink, text)
    }

    SelectLineOfBusiness(item) {
        return super.SelectItem(this._officeLobItem, item)

    }

    EnterCompanyName(text) {
        return super.EnterData(this._companyInput, text)

    }

    SelectRegion(item) {
        return super.SelectItem(this._regionItem, item)

    }

    SelectCountry(item) {
        return super.SelectItem(this._contryItem, item)

    }

    EnterCity(city) {
        return super.EnterData(this._cityItem, city)
    }

    ClickAddToCreateNonDirectory() {
        browser.pause(2000)
        this._addNonDirectoryOfficeBtn.click()
        browser.pause(3000)
        if (this._okButtnFromSuccessDialog.isDisplayed()) {
            this._okButtnFromSuccessDialog.click()
        }
    }

    VerifyNonDirectoryOfficeIsAdded(officename) {
        return super.IsElementPresent($(`//a[contains(text(),'${officename}')]`))
    }

    ClickOnSortByOfficeTypeDropDown(value) {
        return super.SelectItem(this._sortByOfficeType, value)
    }

    VerifySortingByOfficeType(item) {
        return super.IsElementPresent($(`=${item}`))
    }

    EnterTextToFilter(filter) {

        return super.EnterData(this._filterOffice, filter)
    }

    ClickOnAssociateOffice(option) {
        browser.pause(2000)
        return $(`//a[contains(text(),'${option}')]`).click()
    }

    ChooseOption(value) {
        super.SelectItem($("//div[@class='editable-input']//select"), value)
        return super.ClickOn($("//button[@class='editable-submit']"))
    }

    VerifyAssociateOffice(option) {
        return super.IsElementPresent($(`//a[contains(text(),'${option}')]`))
    }

    ClickOnSyncOffice() {
        browser.pause(2000)
        return this._syncOffice.click()
    }

    VerifyOfficeIsSynced() {

        browser.pause(3000)
        return this._syncVerifyMessage.isDisplayed()
    }

    ClickOkFromSyncDialog() {
        browser.pause(2000)
        return this._syncOkButtn.click()
    }


}

export default new OfficeAdmin()