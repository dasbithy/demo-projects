import CasLogin from '../pages/login.page'
import ClientCompany from '../pages/clientCompany.page'
import ClientContact from '../pages/clientContact.page'
import SearchItem from '../pages/searchHome.page'
import Activity from '../pages/activity.page'
import Opportunity from '../pages/opportunity.page'
import Browse from '../pages/browse.page'
import UserAdmin from '../pages/userAdmin.page'
import OfficeAdmin from '../pages/officeAdmin.page'
import ReportExport from '../pages/report.page'
import Export from '../pages/export.page'
import Revenue from '../pages/revenue.page'
import Metadata from '../pages/metadata.page'
import CleanUp from '../pages/cleanup.page'


describe(`${siteEnv} ::`, () => {

    //
    // ────────────────────────────────────────────────────────────────────────────  ──────────
    //   :::::: C A S / L O G I N : :  :   :    :     :        :          :
    // ──────────────────────────────────────────────────────────────────────────────────────
    //
    describe("CAS/LOGIN ", () => {

        it("Verify User can Login to via CAS", () => {

            CasLogin.open('/')
            CasLogin.NavigateToSigninPage()
            CasLogin.WaitForPageToLoad()
            CasLogin.SelectLoginType()
            CasLogin.EnterUserName(username)
            CasLogin.EnterPassword(password)
            CasLogin.SelectDomain(domain)
            CasLogin.HitSubmit()
            CasLogin.RefreshifErrorComesUp()
            expect(CasLogin.SuccessfullLoggedIn()).to.equal("Logout")
            expect(CasLogin.VerifyMyOfficeAppearsAfterLogin()).to.include("My Office")
        })

    })
   
    //
    // ──────────────────────────────────────────────────────────────────── I ──────────
    //   :::::: C L I E N T   C O M P A N Y : :  :   :    :     :        :          :
    // ──────────────────────────────────────────────────────────────────────────────
    //
    describe("CLIENT COMPANY", () => {


        it("Verify User can add Client Company", () => {
            ClientCompany.open("/user/Client.action")
            ClientCompany.WaitForPageToLoad()
            ClientCompany.EnterClientCompanyName("CCompany-AngryBird")
            ClientCompany.SelectLob(1)
            ClientCompany.SelectRelation(2)
            ClientCompany.SelectIndustrySector()

        })

    })

    //
    // ──────────────────────────────────────────────────────────────────── II ──────────
    //   :::::: C L I E N T   C O N T A C T : :  :   :    :     :        :          :
    // ──────────────────────────────────────────────────────────────────────────────
    //
    describe("CLIENT CONTACT", () => {

        it("Verify User can add Client Contact", () => {
            ClientContact.open("/user/Contact.action")
            ClientContact.WaitForPageToLoad()
            ClientContact.EnterClientCompanyFromSuggestions("CCompany-AngryBird")
            ClientContact.EnterFirstName("AngryBird")
            ClientContact.EnterLastName("ClientContact")
            ClientContact.EnterEmail("lobtest2@yopmail.com")
            ClientContact.ClickOnShareLobLevel()
            expect(ClientContact.VerifyShareWithIndividualAppears()).to.be.true
            ClientContact.EnterAndSelectUserName("test")

        })

    })

    //
    // ────────────────────────────────────────────────────────────────────── III ──────────
    //   :::::: C L I E N T   A C T I V I T Y : :  :   :    :     :        :          :
    // ────────────────────────────────────────────────────────────────────────────────
    //
    describe("ACTIVITY ", () => {

        it("Verify user can add activity", () => {
            Activity.open("/user/Activity.action")
            Activity.WaitForPageToLoad()
            Activity.EnterActivityTitle("Automation Activity")
            Activity.SelectLob(2)
            Activity.SelectActivityType(3)
            Activity.SelectActivityFrom("17-July-2019")
            Activity.SelectActivityTo("20-July-2019")
            Activity.EnterDescription("I am on automation activity")

        })

    })

    //
    // ────────────────────────────────────────────────────────────── IV ──────────
    //   :::::: O P P O R T U N I T Y : :  :   :    :     :        :          :
    // ────────────────────────────────────────────────────────────────────────
    //

    describe("OPPORTUNITY ", () => {

        it("Verify selecting 'Yes' for existing client shows incremental client opportunities warning", () => {
            Opportunity.Open("/user/Prospect.action")
            Opportunity.WaitForPageToLoad()
            //     Opportunity.EnterClientCompany("AngryBird-EDITED")
            //     Opportunity.SelectClientCompanyFromSuggestion()
            //     Opportunity.SelectExistingClientNo()
            //     expect(Opportunity.VerifyNoAlertDisplayed()).to.be.false
            //     Opportunity.SelectExistingClientYes()
            //     expect(Opportunity.VerifyAlertDisplayed()).to.be.true
            // })

            // it("Verify clicking on 'Select Brand' shows Multi-Select metadata", ()=>{
            //     Opportunity.ClickOnSelectBrand()
            //     expect(Opportunity.VerifyMultiSelectOptionDisplayed()).to.be.true
            // })

            // it("Verify Selecting Opportunity type 'Chasing', Detailed Information metadata does not appear", ()=>{
            //     Opportunity.SelectItemFromMultiSelect()
            //     Opportunity.AddtoAssigned()
            //     Opportunity.EnterClientContact("AngryBird ClientContact")
            //     Opportunity.SelectContactFromSuggestion()
            //     Opportunity.EnterOpportunityName("AngryBird-Opportunity")
            //     Opportunity.SelectOppoType(1)
            //     expect(Opportunity.VerifyDetailedInformationAppeared()).to.be.false
            // })

            // it("Verify Selecting Opportunity type 'Impressing', Detailed Information metadata appears", ()=>{

            //     Opportunity.SelectOppoType(2)
            //     expect(Opportunity.VerifyDetailedInformationAppeared()).to.be.true

            // })

            // it("Verify Selecting Opportunity type 'Winning', Detailed Information metadata appears", ()=>{
            //     Opportunity.SelectOppoType(3)
            //     expect(Opportunity.VerifyDetailedInformationAppeared()).to.be.true
            // })

            // it("Verify Selecting Opportunity type 'Winning', Detailed Information metadata appears", ()=>{
            //     Opportunity.SelectOriginOfEngagement()
            //     expect(Opportunity.VerifySearchConsultantFieldAppear()).to.be.true
            // })

            // it("Verify Selecting 'Search Consultant' under Impressing and Winning metadata, 'Name of Search Consultant' field appears", ()=>{
            //     Opportunity.SelectOriginOfEngagement()
            //     Opportunity.EnterSearchConsultant("automation")
            //     expect(Opportunity.VerifySearchConsultantFieldAppear()).to.be.true
            // })

            // it("Verify User can create Opportunity in status of 'In Progress'", ()=>{
            //     Opportunity.SelectAudience("B2B")
            //     Opportunity.SelectAudience("B2C")
            //     Opportunity.SelectAudience("Both")
            //     Opportunity.SelectRequiredCapabilities()
            //     Opportunity.SelectNetworkPartner()
            //     Opportunity.EnterTermsOfContact(5)
            //     Opportunity.EnterDecisionDate(15)
            //     Opportunity.EnterEstimatedDate("Dec")
            //     Opportunity.SelectIndustry()
            //     Opportunity.SelectPrimaryOffice()//Depends on Environment-------------------->
            //     Opportunity.EnterOppoDescription("Opportunity Description...")
            //     Opportunity.EnterPotentialAnnualizedRevenueNumber(500)
            //     Opportunity.SelectProgressStatus('In Progress')
            //     Opportunity.SelectProbability(50)
            //     Opportunity.SelectCurrentStage()
            //     Opportunity.EnterWhatHappensNext("Won by Automation script")
            //     Opportunity.SelectNextStage()
            //     Opportunity.SelectNextKeyDate(25)

        })

    })

    //
    // ──────────────────────────────────────────────────────────────────────────── V ──────────
    //   :::::: B R O W S E   O P P O R T U N I T Y : :  :   :    :     :        :          :
    // ──────────────────────────────────────────────────────────────────────────────────────
    //
    describe("BROWSE ", () => {

        it("Verify browse opportunity by office", () => {
            Browse.Open("user/BrowseProspects.action")
            Browse.ClickOnByOffice()
            expect(Browse.VerifyOpportunitiesByOffice()).to.be.true
            expect(Browse.VerifyRecordsFoundByOffice()).to.be.above(0)
        })

        it("Verify browse opportunity by Client Company", () => {
            //Browse.Open("user/BrowseProspects.action")
            Browse.ClickOnByClientCompany()
            expect(Browse.VerifyOpportunitiesByClientCompany()).to.be.true
            expect(Browse.VerifyRecrodsFoundByClientCompany()).to.be.above(0)
        })



        it("Verify browse opportunity by Industry", () => {
            // Browse.Open("user/BrowseProspects.action")
            Browse.ClickOnByIndustry()
            expect(Browse.VerifyOpportunitiesByIndustry()).to.be.true
            expect(Browse.VerifyRecrodsFoundByIndustry()).to.be.above(0)
        })

        it("Verify browse opportunity by Archive", () => {
            //Browse.Open("user/BrowseProspects.action")
            Browse.ClickOnByArchive()
            expect(Browse.VerifyOpportunitiesByArchive()).to.be.true
            expect(Browse.VerifyRecrodsFoundByArchive(), "Currently Archive has no item").to.be.above(0)
        })

        it("Verify browse opportunity by Recent Activity", () => {
            //Browse.Open("user/BrowseProspects.action")
            Browse.ClickOnByRecentActivity()
            expect(Browse.VerifyOpportunitiesByRecentActivity()).to.be.true
            expect(Browse.VerifyRecrodsFoundByRecentActivity(), "Currently Recent Activity has no item").to.be.above(0)
        })

        it("Verify browse Client Companies", () => {
            Browse.Open("/user/SearchClients.action")
            Browse.EnterClientCompanyName("Test")
            Browse.SelectLobFromDropDown(1)
            Browse.ClickOnSearch()
            expect(Browse.VerifyClientCompanyAppearsBySearch()).to.be.include("Test")
            expect(Browse.VerifyResultsCount()).to.be.above(0)
        })

        it("Verify After search User can export client Companies to excel", () => {
            Browse.ClickOnExport()
            expect(Browse.VerifyFileIsDownloaded("Clients.xls")).to.be.include("te")
        })



        it("Verify browse Client Contact", () => {
            Browse.Open("/user/SearchContacts.action")
            Browse.EnterClientContactName("Te")
            Browse.ClickOnSearch()
            expect(Browse.VerifyClientContactAppears()).to.be.include("Te")
            expect(Browse.VerifyResultsCount()).to.be.above(0)
        })

        it("Verify After search User can export client Contact to excel", () => {
            Browse.ClickOnExport()
            expect(Browse.VerifyFileIsDownloaded("Contacts.xls")).to.be.include("te")
        })

        it("Verify browse Activity", () => {
            Browse.Open("/user/SearchActivities.action")
            Browse.EnterActivityName("Test")
            Browse.ClickOnSearch()
            expect(Browse.VerifyClientContactAppears()).to.be.include("Test")
            expect(Browse.VerifyResultsCount()).to.be.above(0)
        })

        it("Verify After search User can export Activities to excel", () => {
            Browse.ClickOnExport()
            expect(Browse.VerifyFileIsDownloaded("Activities.xls")).to.be.include("Test")
        })

    })


    //
    // ──────────────────────────────────────────────────────────── VI ──────────
    //   :::::: U S E R   A D M I N : :  :   :    :     :        :          :
    // ──────────────────────────────────────────────────────────────────────
    //

    describe("MANAGE >> USER ADMIN", () => {
        it("Verify Directory user can be added to New BusinessGroup", () => {

            UserAdmin.Open("/user/Subscriptions.action")
            //UserAdmin.ClickOnManageMenu()
            UserAdmin.WaitForPageToLoad()
            UserAdmin.ClickOnUserAdminTab()
            UserAdmin.WaitForUserAdminPageToLoad()
            UserAdmin.ClickOnAddNewUserToNewBusinessGroup()
            expect(UserAdmin.VerifyModalAppear()).to.be.true
            UserAdmin.ClickOnDirectoryType()
            UserAdmin.EnterNametoSelectUser("Mithun")


        })



        it("Verify Non-Directory user can be added to New BusinessGroup", () => {

            UserAdmin.Open("/user/Subscriptions.action")
            //UserAdmin.ClickOnManageMenu()
            UserAdmin.WaitForPageToLoad()
            UserAdmin.ClickOnUserAdminTab()
            UserAdmin.WaitForUserAdminPageToLoad()
            UserAdmin.ClickOnAddNewUserToNewBusinessGroup()
            expect(UserAdmin.VerifyModalAppear()).to.be.true
            UserAdmin.ClickOnNonDirectoryType()
            UserAdmin.EnterNametoSelectUser("swp.yr")
            UserAdmin.SelectOffice()
            UserAdmin.EnterUpn("123456@yopmail.com")


        })

        it("Verify User can filter users", () => {
            UserAdmin.Open("/user/Subscriptions.action")
            //UserAdmin.ClickOnManageMenu()
            UserAdmin.WaitForPageToLoad()
            UserAdmin.ClickOnUserAdminTab()
            UserAdmin.WaitForUserAdminPageToLoad()
            UserAdmin.EnterFilterText("swp.yr")
            expect(UserAdmin.VerifyFilterUserIsDisplayed("swp.yr@wunderman.com")).to.be.true
        })

        it("Verify user can download from user admin", () => {
            UserAdmin.Open("/user/Subscriptions.action")
            //UserAdmin.ClickOnManageMenu()
            UserAdmin.WaitForPageToLoad()
            UserAdmin.ClickOnUserAdminTab()
            UserAdmin.WaitForUserAdminPageToLoad()
            //UserAdmin.Open("/user/User.action")
            UserAdmin.ClickOnDownloadLink()
            expect(Browse.VerifyFileIsDownloaded("Users.xls")).to.be.include("swp")
        })

    })

    //
    // ──────────────────────────────────────────────────────────────── VII ──────────
    //   :::::: O F F I C E   A D M I N : :  :   :    :     :        :          :
    // ──────────────────────────────────────────────────────────────────────────
    //


    describe("MANAGE >> OFFICE ADMIN", () => {
        it("Verify User can add Directory office", () => {
            UserAdmin.Open("/user/Subscriptions.action")
            //UserAdmin.ClickOnManageMenu()
            OfficeAdmin.ClickOnOfficeAdminTab()
            OfficeAdmin.WaitForPageToLoad()
            OfficeAdmin.ClickOnAddOffice()
            expect(OfficeAdmin.VerifyOfficeFormAppears()).to.be.true
            OfficeAdmin.SelectDirectoryAsOfficeType()
            OfficeAdmin.EnterOfficeName("test")

        })

        it("Verify User can add Non-Directory Office", () => {
            UserAdmin.Open("/user/Subscriptions.action")
            //UserAdmin.ClickOnManageMenu()
            OfficeAdmin.ClickOnOfficeAdminTab()
            OfficeAdmin.WaitForPageToLoad()
            OfficeAdmin.ClickOnAddOffice()
            expect(OfficeAdmin.VerifyOfficeFormAppears()).to.be.true
            OfficeAdmin.ClickOnNonDirectoryType()
            OfficeAdmin.EnterOfficeName("AutomateMe")
            OfficeAdmin.SelectLineOfBusiness(1)
            OfficeAdmin.EnterCompanyName("AutomateMeCompany")
            OfficeAdmin.SelectRegion(2)
            OfficeAdmin.SelectCountry("Bangladesh")
            OfficeAdmin.EnterCity("Dhaka")

        })

        it("Verify User can sort by Directory office type", () => {
            OfficeAdmin.Open("/user/OfficeAdmin.action")
            OfficeAdmin.ClickOnSortByOfficeTypeDropDown(1)
            expect(OfficeAdmin.VerifySortingByOfficeType("Asia")).to.be.true
        })

        it("Verify User can sort by Non-Directory office type", () => {
            OfficeAdmin.ClickOnSortByOfficeTypeDropDown(2)
            expect(OfficeAdmin.VerifySortingByOfficeType("Asia")).to.be.false
        })

        it("Verify User can filter office type", () => {
            OfficeAdmin.Open("/user/OfficeAdmin.action")
            OfficeAdmin.EnterTextToFilter("WUNDERMAN THOMPSON")
            expect(OfficeAdmin.VerifySortingByOfficeType("Asia")).to.be.true
        })

    })

    //
    // ──────────────────────────────────────────────────── VIII ──────────
    //   :::::: R E P O R T S: :  :   :    :     :        :          :
    // ──────────────────────────────────────────────────────────────
    //

    describe("REPORTS ", () => {
        it("Verify User can download Wins Report", () => {
            ReportExport.Open("/user/Report.action")
            ReportExport.WaitForPageToLoad()
            ReportExport.ClickOnLobTab('Y&R')
            ReportExport.ClickOnWinsReport()
            ReportExport.ClickOnExportThisReport()
            expect(ReportExport.VerifyReportIsDownloaded("NBTExport_Wins.xls")).to.be.include(ReportExport.WinContents())
        })

        it("Verify User can download In Progress Report", () => {
            ReportExport.Open("/user/Report.action")
            ReportExport.WaitForPageToLoad()
            ReportExport.ClickOnLobTab('Y&R')
            ReportExport.ClickOnProgressReport()
            ReportExport.ClickOnExportThisReport()
            expect(ReportExport.VerifyReportIsDownloaded("NBTExport_InProgressReport.xls")).to.be.include(ReportExport.ProgressContents())
        })

        it("Verify User can download Activity Report", () => {
            ReportExport.Open("/user/Report.action")
            ReportExport.WaitForPageToLoad()
            ReportExport.ClickOnActivityReport()
            ReportExport.ClickOnExportThisReport()
            expect(ReportExport.VerifyReportIsDownloaded("NBTExport_Activity.xls")).to.be.include(ReportExport.ActivityContents())
        })

        it("Verify User can download Stage Analysis Report", () => {
            ReportExport.Open("/user/Report.action")
            ReportExport.WaitForPageToLoad()
            ReportExport.ClickOnStageAnalysisReport()
            ReportExport.ClickOnExportThisReport()
            expect(ReportExport.VerifyReportIsDownloaded("NBTExport_StageAnalysis.xls")).to.be.include(ReportExport.StageAnalysisContents())
        })

        it("Verify User can download Monthly Management Report", () => {
            ReportExport.Open("/user/Report.action")
            ReportExport.WaitForPageToLoad()
            ReportExport.ClickOnMonthlyManagement()
            ReportExport.ClickOnExportThisReport()
            expect(ReportExport.VerifyReportIsDownloaded("MonthlyManagementReportExport.xls")).to.be.include("Collaborator")
        })

        it("Verify User can download Initiation Report", () => {
            ReportExport.Open("/user/Report.action")
            ReportExport.WaitForPageToLoad()
            ReportExport.ClickOnInitiation()
            ReportExport.ClickOnExportThisReport()
            expect(ReportExport.VerifyReportIsDownloaded("NBTExport_Initiation.xls")).to.be.include(ReportExport.InitiationContents())
        })

        it("Verify User can download InitiationYOY Report", () => {
            ReportExport.Open("/user/Report.action")
            ReportExport.WaitForPageToLoad()
            ReportExport.ClickOnInitiationYOY()
            ReportExport.ClickOnExportThisReport()
            expect(ReportExport.VerifyReportIsDownloaded("NBTExport_InitiationYoY.xls")).to.be.include(ReportExport.InitiationYOYContents())
        })

        it("Verify User can download ConsultYOY Report", () => {
            ReportExport.Open("/user/Report.action")
            ReportExport.WaitForPageToLoad()
            ReportExport.ClickOnConsultYoY()
            ReportExport.ClickOnExportThisReport()
            expect(ReportExport.VerifyReportIsDownloaded("NBTExport_ConsultantYoY.xls")).to.be.include("% Change YoY")
        })

        it("Verify User can download Pipeline Report", () => {
            ReportExport.Open("/user/Report.action")
            ReportExport.WaitForPageToLoad()
            ReportExport.ClickOnPipelineReport()
            ReportExport.ClickOnExportThisReport()
            expect(ReportExport.VerifyReportIsDownloaded("PipelineReportExport.xls")).to.be.include(ReportExport.PipelineContents())
        })

        it("Verify User can download Win Survey Report", () => {
            ReportExport.Open("/user/Report.action")
            ReportExport.WaitForPageToLoad()
            ReportExport.ClickOnWinSurvey()
            ReportExport.ClickOnExportThisReport()
            expect(ReportExport.VerifyReportIsDownloaded("SurveyReportExport.xls")).to.be.include(ReportExport.WinSurveyContents())
        })

        it("Verify User can download Loss Survey Report", () => {
            ReportExport.Open("/user/Report.action")
            ReportExport.WaitForPageToLoad()
            ReportExport.ClickOnLossSurvey()
            ReportExport.ClickOnExportThisReport()
            expect(ReportExport.VerifyReportIsDownloaded("SurveyReportExport (1).xls")).to.be.include(ReportExport.LossSurveyContents())
        })
    })

    //
    // ────────────────────────────────────────────────────── IX ──────────
    //   :::::: E X P O R T S : :  :   :    :     :        :          :
    // ────────────────────────────────────────────────────────────────
    //

    describe("EXPORT ", () => {
        it("Verify User Can create View", () => {
            Export.Open("/user/Export.action")
            Export.WaitForPageLoad()
            Export.ClickOnViewMenu()

        })

        it("Verify User Filter Column", () => {
            Export.Open("/user/Export.action")
            Export.WaitForPageLoad()
            Export.ClickOnColumnSelector()
            Export.SelectFilter()
            //Export.CloseColumnFilter()
            expect(Export.VerifyColumFilterIsSet()).to.be.above(30)
        })

        it("Verify User can do custom export Excel File ", () => {
            Export.Open("/user/Export.action")
            Export.WaitForPageLoad()
            Export.ClickOnExportLink()
            Export.ClickOnDownloadXLSX()
            expect(Export.VerifyFileIsDownloaded("NBTExport.xls")).to.be.include("ID")
        })

        it("Verify User can do custom export PDF File ", () => {
            Export.Open("/user/Export.action")
            Export.WaitForPageLoad()
            Export.ClickOnExportLink()
            Export.ClickOnDownloadPDF()
            expect(Export.VerifyFileIsDownloaded("NBTExport.pdf")).to.be.include("ID")
        })




    })

    //
    // ────────────────────────────────────────────────────────────────────── X ──────────
    //   :::::: V A L I D A T E  R E V E N U E : :  :   :    :     :        :          :
    // ────────────────────────────────────────────────────────────────────────────────
    //

    describe("VALIDATE REVENUE ", () => {
        it("Verify User can Download Validate Revenue Report", () => {
            Revenue.Open("/user/ValidateRevenue.action")
            Revenue.WaitForPageLoad()
            expect(Revenue.VerifyValidateRevenuePage()).to.be.true
            Revenue.ClickOnExportButton()
            expect(Revenue.VerifyFileIsDownloaded("ValidateRevenueExport.xls")).to.be.include(Revenue.GetItemFromTable())
        })
    })

    //
    // ──────────────────────────────────────────────────────── XI ──────────
    //   :::::: M E T A D A T A : :  :   :    :     :        :          :
    // ──────────────────────────────────────────────────────────────────
    //
    describe("METADATA ", () => {

        it("Verify Metadata appears in Opportunity Page", () => {
            Metadata.Open("/user/Metadata.action")
            Metadata.WaitForPageToLoad()
            Metadata.ClickOnLob("WUNDERMANTHOMPSON")
            Metadata.ClickOnAddMetadata()
            Metadata.EnterFieldName("AutomateMe-TextField")
            Metadata.SelectFieldType(0)
            Metadata.IsItRequiredCheckBox(true)

        })

        it("Verify User can create SelectBox Metadata", () => {
            Metadata.Open("/user/Metadata.action")
            Metadata.WaitForPageToLoad()
            Metadata.ClickOnLob("WUNDERMANTHOMPSON")
            Metadata.ClickOnAddMetadata()
            expect(Metadata.VerifyAddFieldDialogAppears()).to.be.true
            Metadata.EnterFieldName("AutomateMe-SelectBox")
            Metadata.SelectFieldType(1)
            Metadata.IsItRequiredCheckBox(true)

        })

        it("Verify User can create MultiSelectBox Metadata", () => {
            Metadata.Open("/user/Metadata.action")
            Metadata.WaitForPageToLoad()
            Metadata.ClickOnLob("WUNDERMANTHOMPSON")
            Metadata.ClickOnAddMetadata()
            expect(Metadata.VerifyAddFieldDialogAppears()).to.be.true
            Metadata.EnterFieldName("AutomateMe-MultiSelectBox")
            Metadata.SelectFieldType(2)
            Metadata.IsItRequiredCheckBox(true)

        })

        it("Verify User can create CheckBoxList Metadata", () => {
            Metadata.Open("/user/Metadata.action")
            Metadata.WaitForPageToLoad()
            Metadata.ClickOnLob("WUNDERMANTHOMPSON")
            Metadata.ClickOnAddMetadata()
            expect(Metadata.VerifyAddFieldDialogAppears()).to.be.true
            Metadata.EnterFieldName("AutomateMe-CheckBoxList")
            Metadata.SelectFieldType(3)
            Metadata.IsItRequiredCheckBox(true)

        })

        it("Verify User can create Number Metadata", () => {
            Metadata.Open("/user/Metadata.action")
            Metadata.WaitForPageToLoad()
            Metadata.ClickOnLob("WUNDERMANTHOMPSON")
            Metadata.ClickOnAddMetadata()
            expect(Metadata.VerifyAddFieldDialogAppears()).to.be.true
            Metadata.EnterFieldName("AutomateMe-Number")
            Metadata.SelectFieldType(4)
            Metadata.IsItRequiredCheckBox(true)

        })

        it("Verify User can create Dollar Amount Metadata", () => {
            Metadata.Open("/user/Metadata.action")
            Metadata.WaitForPageToLoad()
            Metadata.ClickOnLob("WUNDERMANTHOMPSON")
            Metadata.ClickOnAddMetadata()
            expect(Metadata.VerifyAddFieldDialogAppears()).to.be.true
            Metadata.EnterFieldName("AutomateMe-DollarAmount")
            Metadata.SelectFieldType(5)
            Metadata.IsItRequiredCheckBox(true)

        })

    })


    //<!------------------End of SUITE---------------->
})