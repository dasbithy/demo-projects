import CasLogin from '../pages/login.page'
import ClientCompany from '../pages/clientCompany.page'
import ClientContact from '../pages/clientContact.page'
import SearchItem from '../pages/searchHome.page'
import Activity from '../pages/activity.page'
import Opportunity from '../pages/opportunity.page'
import Browse from '../pages/browse.page'
import UserAdmin from '../pages/userAdmin.page'
import OfficeAdmin from '../pages/officeAdmin.page'
import ReportExport from '../pages/report.page'
import Export from '../pages/export.page'
import Revenue from '../pages/revenue.page'
import Metadata from '../pages/metadata.page'
import CleanUp from '../pages/cleanup.page'


describe(`${siteEnv} ::`, () => {

    //
    // ────────────────────────────────────────────────────────────────────────────  ──────────
    //   :::::: C A S / L O G I N : :  :   :    :     :        :          :
    // ──────────────────────────────────────────────────────────────────────────────────────
    //
    describe("NBT CAS/LOGIN ", () => {

        it("Verify User can Login to NBT via CAS", () => {

            CasLogin.open('/')
            CasLogin.NavigateToSigninPage()
            CasLogin.WaitForPageToLoad()
            CasLogin.SelectLoginType()
            CasLogin.EnterUserName(username)
            CasLogin.EnterPassword(password)
            CasLogin.SelectDomain(domain)
            CasLogin.HitSubmit()
            CasLogin.RefreshifErrorComesUp()
            expect(CasLogin.SuccessfullLoggedIn()).to.equal("Logout")
            expect(CasLogin.VerifyMyOfficeAppearsAfterLogin()).to.include("My Office")
        })

    })

        //
    // ────────────────────────────────────────────────────────────────────────────  ──────────
    //   :::::: CLEANUP: :  :   :    :     :        :          :
    // ──────────────────────────────────────────────────────────────────────────────────────
    //
    describe("SETUP", () => {
        it("Opportunity should be deleted", () => {

            try{
                Opportunity.Open("/user/Home.action")
                SearchItem.SelectSearchItem(1)
                SearchItem.EnterSearchText("AngryBird-Opportunity")
                SearchItem.ClickOnGoButton()
                Opportunity.ClickOnSearchedOppo("AngryBird-Opportunity")
                Opportunity.ClickOnSearchedOppoEdit()
                CleanUp.ClickOnDeleteButton()
                SearchItem.SelectSearchItem(1)
                SearchItem.EnterSearchText("AngryBird-Opportunity")
                SearchItem.ClickOnGoButton()
                expect(CleanUp.VerifyOppoIsDeleted("AngryBird-Opportunity")).to.be.false
               
            }catch(e){
                console.log(e)
            }
        })

        it("Client Company should be deleted", () => {

            try{
                Opportunity.Open("/user/Home.action")
                SearchItem.SelectSearchItem(2)
                SearchItem.EnterSearchText("CCompany-AngryBird-EDITED")
                SearchItem.ClickOnGoButton()
                Opportunity.ClickOnSearchedOppo("CCompany-AngryBird-EDITED")
                ClientCompany.ClickOnEditClientCompanyIcon()
                CleanUp.ClickOnDeleteClientCompany()
                SearchItem.SelectSearchItem(2)
                SearchItem.EnterSearchText("CCompany-AngryBird-EDITED")
                SearchItem.ClickOnGoButton()
                expect(CleanUp.VerifyClientCompanyIsDeleted("CCompany-AngryBird-EDITED")).to.be.false
               
            }catch(e){
                console.log(e)
            }
        })
    })
    //
    // ──────────────────────────────────────────────────────────────────── I ──────────
    //   :::::: C L I E N T   C O M P A N Y : :  :   :    :     :        :          :
    // ──────────────────────────────────────────────────────────────────────────────
    //

    describe("CLIENT COMPANY", () => {


        it("Verify User can add Client Company", () => {
            ClientCompany.open("/user/Client.action")
            ClientCompany.WaitForPageToLoad()
            ClientCompany.EnterClientCompanyName("CCompany-AngryBird")
            ClientCompany.SelectLob(1)
            ClientCompany.SelectRelation(2)
            ClientCompany.SelectIndustrySector()
            ClientCompany.ClickOnSaveButton()
            expect(ClientCompany.VerifyClientCompanyIsCreated()).to.include("CCompany-AngryBird")
        })

        it("Verify User can add brand on Client Company Edit Page", () => {
            ClientCompany.ClickOnEditIcon()
            ClientCompany.EnterBrandText("Automate")
            ClientCompany.ClickOnAddBrandBtn()
            expect(ClientCompany.VerifyBrandIsAdded("Automate"))
        })

        it("Verify User can delete brand on Client Company Edit Page", () => {
            ClientCompany.DeleteBrand()
            ClientCompany.ClickOnBrandDeleteAlertOk()
            expect(ClientCompany.VerifyBrandIsDeleted()).to.be.false
        })

        it("Verify User can edit Client Company", () => {
            ClientCompany.EnterClientCompanyName("CCompany-AngryBird-EDITED")
            ClientCompany.EnterBrandText("AutoBrand")
            ClientCompany.ClickOnAddBrandBtn()
            ClientCompany.ClickOnUpdate()
            expect(ClientCompany.VerifyClientCompanyIsCreated()).to.include("EDITED")
            expect(ClientCompany.VerifyClientCompanyIsUpdated()).to.include("AutoBrand")
        })

        it("Verify User can add note to Client Company", () => {
            ClientCompany.ClickOnAddNote()
            ClientCompany.EnterNotes("Automation is going on")
            ClientCompany.ClickSaveNote()
            expect(ClientCompany.VerifyNotesAdded()).to.be.equal("Automation is going on")
        })

        it("Verify User can Edit Notes in Client Company", () => {
            ClientCompany.ClickOnEditButton()
            ClientCompany.EnterNotes("Automation is going on - Edited")
            ClientCompany.ClickSaveNote()
            expect(ClientCompany.VerifyNotesAdded()).to.be.equal("Automation is going on - Edited")
        })

        it("Verify User can make Note Private in Client Company", () => {
            ClientCompany.ClickOnEditButton()
            ClientCompany.EnterNotes("Automation is going on - Private")
            ClientCompany.MakeNotePrivate()
            ClientCompany.ClickSaveNote()
            expect(ClientCompany.VerifyNoteIsPrivate()).to.be.true
        })

        it("Verify User can delete note in Client Company", () => {
            ClientCompany.ClickOnDelete()
            ClientCompany.ClickOnOkButton()
            expect(ClientCompany.VerifyNoteIsDeleted()).to.be.false

        })

        it("Verify User can Upload File in Client Company", () => {
            ClientCompany.ChooseFile("testCompany.txt")
            ClientCompany.ClickOnUpload()
            expect(ClientCompany.VerifyFileIsUploaded()).to.be.true
        })

        it("Verify User can download File in Client Company", () => {
            ClientCompany.ClickOnDownloadButtn()
            expect(ClientCompany.VerifyFileIsDownloaded("testCompany.txt")).to.include("New Business Tracker")
        })

        it("Verify User can Delete File in Client Company", () => {
            ClientCompany.ClickOnDeleteFile()
            ClientCompany.ClickOnOkButton()
            expect(ClientCompany.VerifyFileIsDeleted()).to.be.false
        })

    })

    //
    // ──────────────────────────────────────────────────────────────────── II ──────────
    //   :::::: C L I E N T   C O N T A C T : :  :   :    :     :        :          :
    // ──────────────────────────────────────────────────────────────────────────────
    //
    describe("CLIENT CONTACT", () => {

        it("Verify User can add Client Contact", () => {
            ClientContact.open("/user/Contact.action")
            ClientContact.WaitForPageToLoad()
            ClientContact.EnterClientCompanyFromSuggestions("CCompany-AngryBird")
            ClientContact.EnterFirstName("AngryBird")
            ClientContact.EnterLastName("ClientContact")
            ClientContact.EnterEmail("lobtest2@yopmail.com")
            ClientContact.ClickOnShareLobLevel()
            expect(ClientContact.VerifyShareWithIndividualAppears()).to.be.true
            ClientContact.EnterAndSelectUserName("test")
            ClientContact.ClickOnSave()
            expect(ClientContact.VerifyClientContactIsCreated()).to.include("AngryBird ClientContact")
        })

        it("Verify User can add Note in client contact page", () => {
            ClientContact.open("/")
            SearchItem.SelectSearchItem(3)
            SearchItem.EnterSearchText("AngryBird ClientContact")
            SearchItem.ClickOnGoButton()
            SearchItem.ClickOnClientContact("AngryBird ClientContact")
            SearchItem.WaitForPageToLoad()
            ClientCompany.ClickOnAddNote()
            ClientCompany.EnterNotes("Automation is going on")
            ClientCompany.ClickSaveNote()
            expect(ClientCompany.VerifyNotesAdded()).to.be.equal("Automation is going on")
        })

        it("Verify User can Edit Notes in Client Contact", () => {
            ClientCompany.ClickOnEditButton()
            ClientCompany.EnterNotes("Automation is going on - Edited")
            ClientCompany.ClickSaveNote()
            expect(ClientCompany.VerifyNotesAdded()).to.be.equal("Automation is going on - Edited")
        })

        it("Verify User can make Note Private in Client Contact", () => {
            ClientCompany.ClickOnEditButton()
            ClientCompany.EnterNotes("Automation is going on - Private")
            ClientCompany.MakeNotePrivate()
            ClientCompany.ClickSaveNote()
            expect(ClientCompany.VerifyNoteIsPrivate()).to.be.true
        })

        it("Verify User can delete note in Client Contact", () => {
            ClientCompany.ClickOnDelete()
            ClientCompany.ClickOnOkButton()
            expect(ClientCompany.VerifyNoteIsDeleted()).to.be.false

        })

        it("Verify User can Upload File in Client Contact", () => {
            ClientContact.ChooseFile("testContact.txt")
            ClientContact.ClickOnUploadFile()
            expect(ClientContact.VerifyFileIsUploaded()).to.be.true
        })

        it("Verify User can download File in Client Contact", () => {
            ClientContact.ClickOnDownloadFile()
            expect(ClientContact.VerifyFileIsDownloaded("testContact.txt")).to.include("New Business Tracker")
        })

        it("Verify User can Delete File in Client Contact", () => {
            ClientContact.ClickOnDeleteFile()
            ClientCompany.ClickOnOkButton()
            expect(ClientCompany.VerifyFileIsDeleted()).to.be.false
        })
    })

    //
    // ────────────────────────────────────────────────────────────────────── III ──────────
    //   :::::: C L I E N T   A C T I V I T Y : :  :   :    :     :        :          :
    // ────────────────────────────────────────────────────────────────────────────────
    //
    describe("ACTIVITY ", () => {

        it("Verify user can add activity", () => {
            Activity.open("/user/Activity.action")
            Activity.WaitForPageToLoad()
            Activity.EnterActivityTitle("Automation Activity")
            Activity.SelectLob(2)
            Activity.SelectActivityType(5)
            Activity.SelectActivityFrom("17-July-2019")
            Activity.SelectActivityTo("20-July-2019")
            Activity.EnterDescription("I am on automation activity")
            Activity.ClickOnSave()
            expect(Activity.VerifyActivityIsCreated()).to.include("Automation Activity")
        })

        it("Verify User can add Note in Activity Page", () => {
            ClientCompany.ClickOnAddNote()
            ClientCompany.EnterNotes("Automation is going on")
            ClientCompany.ClickSaveNote()
            expect(ClientCompany.VerifyNotesAdded()).to.be.equal("Automation is going on")
        })

        it("Verify User can Edit Notes in Activity Page", () => {
            ClientCompany.ClickOnEditButton()
            ClientCompany.EnterNotes("Automation is going on - Edited")
            ClientCompany.ClickSaveNote()
            expect(ClientCompany.VerifyNotesAdded()).to.be.equal("Automation is going on - Edited")
        })

        it("Verify User can make Note Private in Activity Page", () => {
            ClientCompany.ClickOnEditButton()
            ClientCompany.EnterNotes("Automation is going on - Private")
            ClientCompany.MakeNotePrivate()
            ClientCompany.ClickSaveNote()
            expect(ClientCompany.VerifyNoteIsPrivate()).to.be.true
        })

        it("Verify User can delete note in Activity Page", () => {
            ClientCompany.ClickOnDelete()
            ClientCompany.ClickOnOkButton()
            expect(ClientCompany.VerifyNoteIsDeleted()).to.be.false

        })


    })

    //
    // ────────────────────────────────────────────────────────────── IV ──────────
    //   :::::: O P P O R T U N I T Y : :  :   :    :     :        :          :
    // ────────────────────────────────────────────────────────────────────────
    //

    describe("OPPORTUNITY ", () => {

        it("Verify selecting 'Yes' for existing client shows incremental client opportunities warning", () => {
            Opportunity.Open("/user/Prospect.action")
            Opportunity.WaitForPageToLoad()
            Opportunity.EnterClientCompany("AngryBird-EDITED")
            Opportunity.SelectClientCompanyFromSuggestion()
            Opportunity.SelectExistingClientNo()
            expect(Opportunity.VerifyNoAlertDisplayed()).to.be.false
            Opportunity.SelectExistingClientYes()
            expect(Opportunity.VerifyAlertDisplayed()).to.be.true
        })

        it("Verify clicking on 'Select Brand' shows Multi-Select metadata", () => {
            Opportunity.ClickOnSelectBrand()
            expect(Opportunity.VerifyMultiSelectOptionDisplayed()).to.be.true
        })

        it("Verify Selecting Opportunity type 'Chasing', Detailed Information metadata does not appear", () => {
            Opportunity.SelectItemFromMultiSelect()
            Opportunity.AddtoAssigned()
            Opportunity.EnterClientContact("AngryBird ClientContact")
            Opportunity.SelectContactFromSuggestion()
            Opportunity.EnterOpportunityName("AngryBird-Opportunity")
            Opportunity.SelectOppoType(1)
            expect(Opportunity.VerifyDetailedInformationAppeared()).to.be.false
        })

        it("Verify Selecting Opportunity type 'Impressing', Detailed Information metadata appears", () => {

            Opportunity.SelectOppoType(2)
            expect(Opportunity.VerifyDetailedInformationAppeared()).to.be.true

        })

        it("Verify Selecting Opportunity type 'Winning', Detailed Information metadata appears", () => {
            Opportunity.SelectOppoType(3)
            expect(Opportunity.VerifyDetailedInformationAppeared()).to.be.true
        })

        it("Verify Selecting Opportunity type 'Winning', Detailed Information metadata appears", () => {
            Opportunity.SelectOriginOfEngagement()
            expect(Opportunity.VerifySearchConsultantFieldAppear()).to.be.true
        })

        it("Verify Selecting 'Search Consultant' under Impressing and Winning metadata, 'Name of Search Consultant' field appears", () => {
            Opportunity.SelectOriginOfEngagement()
            Opportunity.EnterSearchConsultant("automation")
            expect(Opportunity.VerifySearchConsultantFieldAppear()).to.be.true
        })

        it("Verify User can create Opportunity in status of 'In Progress'", () => {
            Opportunity.SelectAudience("B2B")
            Opportunity.SelectAudience("B2C")
            Opportunity.SelectAudience("Both")
            Opportunity.SelectRequiredCapabilities()
            Opportunity.SelectNetworkPartner()
            Opportunity.EnterTermsOfContact(5)
            Opportunity.EnterDecisionDate(15)
            Opportunity.EnterEstimatedDate("Dec")
            Opportunity.SelectIndustry()
            Opportunity.SelectPrimaryOffice() //Depends on Environment-------------------->
            Opportunity.EnterOppoDescription("Opportunity Description...")
            Opportunity.EnterPotentialAnnualizedRevenueNumber(500)
            Opportunity.SelectProgressStatus('In Progress')
            Opportunity.SelectProbability(50)
            Opportunity.SelectCurrentStage()
            Opportunity.EnterWhatHappensNext("Won by Automation script")
            Opportunity.SelectNextStage()
            Opportunity.SelectNextKeyDate(25)
            Opportunity.ClickSaveOpportunity()
            expect(Opportunity.VerifyOpportunityIsCreated()).to.be.equal('AngryBird-Opportunity')
        })

        it("Verify Clicking on Edit status open the status popup window", () => {

            Opportunity.ClickOnEditOpportunityStatus()
            expect(Opportunity.VerifyStatusPopupIsOpened()).to.be.true

        })
        //Won,Lost,Dropped,On Hold
        it("Verify User Can create Opportunity in status of 'Won'", () => {

            Opportunity.SelectProgressStatus('Won')
            Opportunity.SelectAddedDate()
            Opportunity.SelectCloseDate()
            Opportunity.EnterEstimatedRevenue("5000")
            Opportunity.ClickOnSaveButtonFromPopUp()
            expect(Opportunity.VerifyOpportunityStatus()).to.be.include('Win')

        })

        it("Verify User Can create Opportunity in status of 'Lost'", () => {
            Opportunity.ClickOnEditOpportunityStatus()
            Opportunity.SelectProgressStatus('Lost')
            Opportunity.SelectAddedDate()
            Opportunity.SelectCloseDate()
            Opportunity.SelectLostOrDroppedField()
            Opportunity.ClickOnSaveButtonFromPopUp()
            expect(Opportunity.VerifyOpportunityStatus()).to.be.include('Loss')

        })

        it("Verify User Can create Opportunity in status of 'Dropped'", () => {
            Opportunity.ClickOnEditOpportunityStatus()
            Opportunity.SelectProgressStatus('Dropped')
            Opportunity.SelectAddedDate()
            Opportunity.SelectCloseDate()
            Opportunity.SelectLostOrDroppedField()
            Opportunity.ClickOnSaveButtonFromPopUp()
            expect(Opportunity.VerifyOpportunityStatus()).to.be.include('Drop')

        })

        it("Verify User Can create Opportunity in status of 'On Hold'", () => {
            Opportunity.ClickOnEditOpportunityStatus()
            Opportunity.SelectProgressStatus('On Hold')
            Opportunity.SelectProbability(50)
            Opportunity.ClickOnSaveButtonFromPopUp()
            expect(Opportunity.VerifyOnHoldStatus()).to.be.true

        })

    })

    //
    // ──────────────────────────────────────────────────────────────────────────── V ──────────
    //   :::::: B R O W S E   O P P O R T U N I T Y : :  :   :    :     :        :          :
    // ──────────────────────────────────────────────────────────────────────────────────────
    //
    describe.skip("BROWSE ", () => {

        it("Verify browse opportunity by office", () => {
            Browse.Open("user/BrowseProspects.action")
            Browse.ClickOnByOffice()
            expect(Browse.VerifyOpportunitiesByOffice()).to.be.true
            expect(Browse.VerifyRecordsFoundByOffice()).to.be.above(0)
        })

        it("Verify browse opportunity by Client Company", () => {
            //Browse.Open("user/BrowseProspects.action")
            Browse.ClickOnByClientCompany()
            expect(Browse.VerifyOpportunitiesByClientCompany()).to.be.true
            expect(Browse.VerifyRecrodsFoundByClientCompany()).to.be.above(0)
        })



        it("Verify browse opportunity by Industry", () => {
            // Browse.Open("user/BrowseProspects.action")
            Browse.ClickOnByIndustry()
            expect(Browse.VerifyOpportunitiesByIndustry()).to.be.true
            expect(Browse.VerifyRecrodsFoundByIndustry()).to.be.above(0)
        })

        it("Verify browse opportunity by Archive", () => {
            //Browse.Open("user/BrowseProspects.action")
            Browse.ClickOnByArchive()
            expect(Browse.VerifyOpportunitiesByArchive()).to.be.true
            expect(Browse.VerifyRecrodsFoundByArchive(), "Currently Archive has no item").to.be.above(0)
        })

        it("Verify browse opportunity by Recent Activity", () => {
            //Browse.Open("user/BrowseProspects.action")
            Browse.ClickOnByRecentActivity()
            expect(Browse.VerifyOpportunitiesByRecentActivity()).to.be.true
            expect(Browse.VerifyRecrodsFoundByRecentActivity(), "Currently Recent Activity has no item").to.be.above(0)
        })

        it("Verify browse Client Companies", () => {
            Browse.Open("/user/SearchClients.action")
            Browse.EnterClientCompanyName("CCompany-AngryBird")
            Browse.SelectLobFromDropDown(1)
            Browse.ClickOnSearch()
            expect(Browse.VerifyClientCompanyAppearsBySearch()).to.be.include("CCompany-AngryBird-EDITED")
            expect(Browse.VerifyResultsCount()).to.be.above(0)
        })

        it("Verify After search User can export client Companies to excel", () => {
            Browse.ClickOnExport()
            expect(Browse.VerifyFileIsDownloaded("Clients.xls")).to.be.include("CCompany-AngryBird-EDITED")
        })



        it("Verify browse Client Contact", () => {
            Browse.Open("/user/SearchContacts.action")
            Browse.EnterClientContactName("AngryBird ClientContact")
            Browse.ClickOnSearch()
            expect(Browse.VerifyClientContactAppears()).to.be.include("AngryBird ClientContact")
            expect(Browse.VerifyResultsCount()).to.be.above(0)
        })

        it("Verify After search User can export client Contact to excel", () => {
            Browse.ClickOnExport()
            expect(Browse.VerifyFileIsDownloaded("Contacts.xls")).to.be.include("AngryBird ClientContact")
        })

        it("Verify browse Activity", () => {
            Browse.Open("/user/SearchActivities.action")
            Browse.EnterActivityName("Automation Activity")
            Browse.ClickOnSearch()
            expect(Browse.VerifyClientContactAppears()).to.be.include("Automation Activity")
            expect(Browse.VerifyResultsCount()).to.be.above(0)
        })

        it("Verify After search User can export Activities to excel", () => {
            Browse.ClickOnExport()
            expect(Browse.VerifyFileIsDownloaded("Activities.xls")).to.be.include("Automation Activity")
        })

    })


    //
    // ──────────────────────────────────────────────────────────── VI ──────────
    //   :::::: U S E R   A D M I N : :  :   :    :     :        :          :
    // ──────────────────────────────────────────────────────────────────────
    //

    describe.skip("MANAGE >> USER ADMIN", () => {
        it("Verify Directory user can be added to New BusinessGroup", () => {

            UserAdmin.Open("/user/Subscriptions.action")
            //UserAdmin.ClickOnManageMenu()
            UserAdmin.WaitForPageToLoad()
            UserAdmin.ClickOnUserAdminTab()
            UserAdmin.WaitForUserAdminPageToLoad()
            UserAdmin.ClickOnAddNewUserToNewBusinessGroup()
            expect(UserAdmin.VerifyModalAppear()).to.be.true
            UserAdmin.ClickOnDirectoryType()
            UserAdmin.EnterNametoSelectUser("Mithun")
            UserAdmin.ClickOnAdd()
            expect(UserAdmin.VerifyUserSuccessDialogIsDisplayed()).to.be.true
            UserAdmin.ClickOnOkFromDialog()
            expect(UserAdmin.VerifyDirectoryUserIsAdded("mithun.sarker@wunderman.com")).to.be.true
        })



        it("Verify Non-Directory user can be added to New BusinessGroup", () => {

            UserAdmin.Open("/user/Subscriptions.action")
            //UserAdmin.ClickOnManageMenu()
            UserAdmin.WaitForPageToLoad()
            UserAdmin.ClickOnUserAdminTab()
            UserAdmin.WaitForUserAdminPageToLoad()
            UserAdmin.ClickOnAddNewUserToNewBusinessGroup()
            expect(UserAdmin.VerifyModalAppear()).to.be.true
            UserAdmin.ClickOnNonDirectoryType()
            UserAdmin.EnterNametoSelectUser("angrybird")
            UserAdmin.SelectOffice()
            UserAdmin.EnterUpn("lobtest3000@yopmail.com")
            UserAdmin.ClickOnAddNonDirectoryUser()
            UserAdmin.ClickOnOkFromDialog()
            expect(UserAdmin.VerifyNonDirectoryUserIsAdded("lobtest3000@yopmail.com")).to.be.true

        })

        it("Verify user can edit User Permission", () => {
            UserAdmin.Open("/user/User.action")
            UserAdmin.WaitForUserAdminPageToLoad()
            UserAdmin.ClickOnEditButton("lobtest3000")
            UserAdmin.WaitForPermissionPopUpToLoad()
            UserAdmin.SelectPermission()
            expect(UserAdmin.VerifyPermissionIsAdded("lobtest3000")).to.be.true

        })

        it("Verify user can remove permission", () => {
            UserAdmin.Open("/user/User.action")
            UserAdmin.WaitForUserAdminPageToLoad()
            UserAdmin.ClickOnEditButton("lobtest3000")
            UserAdmin.WaitForPermissionPopUpToLoad()
            UserAdmin.SelectPermission()
            expect(UserAdmin.VerifyPermissionIsAdded("lobtest3000")).to.be.false

        })

        it("Verify User can filter users", () => {
            UserAdmin.EnterFilterText("lobtest3000")
            expect(UserAdmin.VerifyFilterUserIsDisplayed("lobtest3000@yopmail.com")).to.be.true
        })

        xit("Verify Flag inactive user", () => {
            UserAdmin.ClickOnFlagInactiveUserLink()
            expect(UserAdmin.VerifyStatusInDirectoryColumnAppears()).to.be.true
        })

        it("Verify user can download from user admin", () => {
            //UserAdmin.Open("/user/User.action")
            UserAdmin.ClickOnDownloadLink()
            expect(Browse.VerifyFileIsDownloaded("Users.xls")).to.be.include("lobtest3000")
        })

        it("Verify user can delete Non-Directory user from User Admin", () => {
            UserAdmin.ClickOnDelete("lobtest3000")
            UserAdmin.ClickOnSaveButton()
            expect(UserAdmin.VerifyAddedUserIsDeleted("lobtest3000@yopmail.com")).to.be.false
        })
    })

    //
    // ──────────────────────────────────────────────────────────────── VII ──────────
    //   :::::: O F F I C E   A D M I N : :  :   :    :     :        :          :
    // ──────────────────────────────────────────────────────────────────────────
    //


    describe.skip("MANAGE >> OFFICE ADMIN", () => {
        it("Verify User can add Directory office", () => {
            UserAdmin.Open("/user/Subscriptions.action")
            //UserAdmin.ClickOnManageMenu()
            OfficeAdmin.ClickOnOfficeAdminTab()
            OfficeAdmin.WaitForPageToLoad()
            OfficeAdmin.ClickOnAddOffice()
            expect(OfficeAdmin.VerifyOfficeFormAppears()).to.be.true
            OfficeAdmin.SelectDirectoryAsOfficeType()
            OfficeAdmin.EnterOfficeName("test")
            OfficeAdmin.ClickOnAddBtn()
            expect(OfficeAdmin.VerifyOfficeIsAdded()).to.be.true
        })

        it("Verify User can add Non-Directory Office", () => {
            UserAdmin.Open("/user/Subscriptions.action")
            //UserAdmin.ClickOnManageMenu()
            OfficeAdmin.ClickOnOfficeAdminTab()
            OfficeAdmin.WaitForPageToLoad()
            OfficeAdmin.ClickOnAddOffice()
            expect(OfficeAdmin.VerifyOfficeFormAppears()).to.be.true
            OfficeAdmin.ClickOnNonDirectoryType()
            OfficeAdmin.EnterOfficeName("AutomateMe")
            OfficeAdmin.SelectLineOfBusiness(1)
            OfficeAdmin.EnterCompanyName("AutomateMeCompany")
            OfficeAdmin.SelectRegion(2)
            OfficeAdmin.SelectCountry("Bangladesh")
            OfficeAdmin.EnterCity("Dhaka")
            OfficeAdmin.ClickAddToCreateNonDirectory()
            expect(OfficeAdmin.VerifyNonDirectoryOfficeIsAdded("AutomateMe")).to.be.true
        })

        it("Verify User can sort by Directory office type", () => {
            OfficeAdmin.Open("/user/OfficeAdmin.action")
            OfficeAdmin.ClickOnSortByOfficeTypeDropDown(1)
            expect(OfficeAdmin.VerifySortingByOfficeType("AutomateMe")).to.be.false
        })

        it("Verify User can sort by Non-Directory office type", () => {
            OfficeAdmin.ClickOnSortByOfficeTypeDropDown(2)
            expect(OfficeAdmin.VerifySortingByOfficeType("AutomateMe")).to.be.true
        })

        it("Verify User can filter office type", () => {
            OfficeAdmin.Open("/user/OfficeAdmin.action")
            OfficeAdmin.EnterTextToFilter("AutomateMe")
            expect(OfficeAdmin.VerifySortingByOfficeType("AutomateMe")).to.be.true
        })

        it("Verify User can set Yes and No to determine the Associate office", () => {
            OfficeAdmin.Open("/user/OfficeAdmin.action")
            OfficeAdmin.EnterTextToFilter("AutomateMe")
            OfficeAdmin.ClickOnAssociateOffice('No')
            OfficeAdmin.ChooseOption(1)
            expect(OfficeAdmin.VerifyAssociateOffice('Yes')).to.be.true
            OfficeAdmin.ClickOnAssociateOffice('Yes')
            OfficeAdmin.ChooseOption(0)
            expect(OfficeAdmin.VerifyAssociateOffice('No')).to.be.true
        })

        it("Verify user can sync office", () => {
            OfficeAdmin.Open("/user/OfficeAdmin.action")
            OfficeAdmin.ClickOnSyncOffice()
            expect(OfficeAdmin.VerifyOfficeIsSynced()).to.be.true
            OfficeAdmin.ClickOkFromSyncDialog()
        })


    })

    //
    // ──────────────────────────────────────────────────── VIII ──────────
    //   :::::: R E P O R T S: :  :   :    :     :        :          :
    // ──────────────────────────────────────────────────────────────
    //

    describe.skip("REPORTS ", () => {
        it("Verify User can download Wins Report", () => {
            ReportExport.Open("/user/Report.action")
            ReportExport.WaitForPageToLoad()
            ReportExport.ClickOnLobTab('Y&R')
            ReportExport.ClickOnWinsReport()
            ReportExport.ClickOnExportThisReport()
            expect(ReportExport.VerifyReportIsDownloaded("NBTExport_Wins.xls")).to.be.include(ReportExport.WinContents())
        })

        it("Verify User can download In Progress Report", () => {
            ReportExport.Open("/user/Report.action")
            ReportExport.WaitForPageToLoad()
            ReportExport.ClickOnLobTab('Y&R')
            ReportExport.ClickOnProgressReport()
            ReportExport.ClickOnExportThisReport()
            expect(ReportExport.VerifyReportIsDownloaded("NBTExport_InProgressReport.xls")).to.be.include(ReportExport.ProgressContents())
        })

        it("Verify User can download Activity Report", () => {
            ReportExport.Open("/user/Report.action")
            ReportExport.WaitForPageToLoad()
            ReportExport.ClickOnActivityReport()
            ReportExport.ClickOnExportThisReport()
            expect(ReportExport.VerifyReportIsDownloaded("NBTExport_Activity.xls")).to.be.include(ReportExport.ActivityContents())
        })

        it("Verify User can download Stage Analysis Report", () => {
            ReportExport.Open("/user/Report.action")
            ReportExport.WaitForPageToLoad()
            ReportExport.ClickOnStageAnalysisReport()
            ReportExport.ClickOnExportThisReport()
            expect(ReportExport.VerifyReportIsDownloaded("NBTExport_StageAnalysis.xls")).to.be.include(ReportExport.StageAnalysisContents())
        })

        it("Verify User can download Monthly Management Report", () => {
            ReportExport.Open("/user/Report.action")
            ReportExport.WaitForPageToLoad()
            ReportExport.ClickOnMonthlyManagement()
            ReportExport.ClickOnExportThisReport()
            expect(ReportExport.VerifyReportIsDownloaded("MonthlyManagementReportExport.xls")).to.be.include("Collaborator")
        })

        it("Verify User can download Initiation Report", () => {
            ReportExport.Open("/user/Report.action")
            ReportExport.WaitForPageToLoad()
            ReportExport.ClickOnInitiation()
            ReportExport.ClickOnExportThisReport()
            expect(ReportExport.VerifyReportIsDownloaded("NBTExport_Initiation.xls")).to.be.include(ReportExport.InitiationContents())
        })

        it("Verify User can download InitiationYOY Report", () => {
            ReportExport.Open("/user/Report.action")
            ReportExport.WaitForPageToLoad()
            ReportExport.ClickOnInitiationYOY()
            ReportExport.ClickOnExportThisReport()
            expect(ReportExport.VerifyReportIsDownloaded("NBTExport_InitiationYoY.xls")).to.be.include(ReportExport.InitiationYOYContents())
        })

        it("Verify User can download ConsultYOY Report", () => {
            ReportExport.Open("/user/Report.action")
            ReportExport.WaitForPageToLoad()
            ReportExport.ClickOnConsultYoY()
            ReportExport.ClickOnExportThisReport()
            expect(ReportExport.VerifyReportIsDownloaded("NBTExport_ConsultantYoY.xls")).to.be.include("% Change YoY")
        })

        it("Verify User can download Pipeline Report", () => {
            ReportExport.Open("/user/Report.action")
            ReportExport.WaitForPageToLoad()
            ReportExport.ClickOnPipelineReport()
            ReportExport.ClickOnExportThisReport()
            expect(ReportExport.VerifyReportIsDownloaded("PipelineReportExport.xls")).to.be.include(ReportExport.PipelineContents())
        })

        it("Verify User can download Win Survey Report", () => {
            ReportExport.Open("/user/Report.action")
            ReportExport.WaitForPageToLoad()
            ReportExport.ClickOnWinSurvey()
            ReportExport.ClickOnExportThisReport()
            expect(ReportExport.VerifyReportIsDownloaded("SurveyReportExport.xls")).to.be.include(ReportExport.WinSurveyContents())
        })

        it("Verify User can download Loss Survey Report", () => {
            ReportExport.Open("/user/Report.action")
            ReportExport.WaitForPageToLoad()
            ReportExport.ClickOnLossSurvey()
            ReportExport.ClickOnExportThisReport()
            expect(ReportExport.VerifyReportIsDownloaded("SurveyReportExport (1).xls")).to.be.include(ReportExport.LossSurveyContents())
        })
    })

    //
    // ────────────────────────────────────────────────────── IX ──────────
    //   :::::: E X P O R T S : :  :   :    :     :        :          :
    // ────────────────────────────────────────────────────────────────
    //

    describe.skip("EXPORT ", () => {
        it("Verify User Can create View", () => {
            Export.Open("/user/Export.action")
            Export.WaitForPageLoad()
            Export.ClickOnViewMenu()
            Export.ClickOnAddView()
            Export.EnterViewName("AUTOMATEME VIEW")
            Export.ClickOnCreateView()
            browser.refresh()
            expect(Export.VerifyViewIsCreated()).to.be.include("AUTOMATEME")
        })

        it("Verify User Can delete View", () => {
            Export.Open("/user/Export.action")
            Export.WaitForPageLoad()
            Export.ClickOnDeleteView("AUTOMATEME")
            Export.ClickOnYesFromDialog()
            expect(Export.VerifyViewIsDeleted('AUTOMATEME VIEW')).to.be.false
        })

        it("Verify User Filter Column", () => {
            Export.Open("/user/Export.action")
            Export.WaitForPageLoad()
            Export.ClickOnColumnSelector()
            Export.SelectFilter()
            //Export.CloseColumnFilter()
            expect(Export.VerifyColumFilterIsSet()).to.be.above(30)
        })

        it("Verify User can do custom export Excel File ", () => {
            Export.Open("/user/Export.action")
            Export.WaitForPageLoad()
            Export.ClickOnExportLink()
            Export.ClickOnDownloadXLSX()
            expect(Export.VerifyFileIsDownloaded("NBTExport.xls")).to.be.include("ID")
        })

        it("Verify User can do custom export PDF File ", () => {
            Export.Open("/user/Export.action")
            Export.WaitForPageLoad()
            Export.ClickOnExportLink()
            Export.ClickOnDownloadPDF()
            expect(Export.VerifyFileIsDownloaded("NBTExport.pdf")).to.be.include("ID")
        })




    })

    //
    // ────────────────────────────────────────────────────────────────────── X ──────────
    //   :::::: V A L I D A T E  R E V E N U E : :  :   :    :     :        :          :
    // ────────────────────────────────────────────────────────────────────────────────
    //

    describe.skip("VALIDATE REVENUE ", () => {
        it("Verify User can Download Validate Revenue Report", () => {
            Revenue.Open("/user/ValidateRevenue.action")
            Revenue.WaitForPageLoad()
            expect(Revenue.VerifyValidateRevenuePage()).to.be.true
            Revenue.ClickOnExportButton()
            expect(Revenue.VerifyFileIsDownloaded("ValidateRevenueExport.xls")).to.be.include(Revenue.GetItemFromTable())
        })
    })

    //
    // ──────────────────────────────────────────────────────── XI ──────────
    //   :::::: M E T A D A T A : :  :   :    :     :        :          :
    // ──────────────────────────────────────────────────────────────────
    //
    describe.skip("METADATA ", () => {

        it("Verify User can create Text Field Metadata", () => {
            Metadata.Open("/user/Metadata.action")
            Metadata.WaitForPageToLoad()
            Metadata.ClickOnLob("AUTOMATEME")
            Metadata.ClickOnAddMetadata()
            Metadata.EnterFieldName("AutomateMe-TextField")
            Metadata.SelectFieldType(0)
            Metadata.IsItRequiredCheckBox(false)
            Metadata.ClickOnSave()

        })

        it("Verify User can create SelectBox Metadata", () => {
            Metadata.Open("/user/Metadata.action")
            Metadata.WaitForPageToLoad()
            Metadata.ClickOnLob("AUTOMATEME")
            Metadata.ClickOnAddMetadata()
            expect(Metadata.VerifyAddFieldDialogAppears()).to.be.true
            Metadata.EnterFieldName("AutomateMe-SelectBox")
            Metadata.SelectFieldType(1)
            Metadata.IsItRequiredCheckBox(false)
            Metadata.ClickOnSave()
            expect(Metadata.VerifyMetaDataIscreated("AutomateMe-SelectBox")).to.be.true
        })

        it("Verify User can create MultiSelectBox Metadata", () => {
            // Metadata.Open("/user/Metadata.action")
            // Metadata.WaitForPageToLoad()
            // Metadata.ClickOnLob("AUTOMATEME")
            Metadata.ClickOnAddMetadata()
            expect(Metadata.VerifyAddFieldDialogAppears()).to.be.true
            Metadata.EnterFieldName("AutomateMe-MultiSelectBox")
            Metadata.SelectFieldType(2)
            Metadata.IsItRequiredCheckBox(false)
            Metadata.ClickOnSave()
            expect(Metadata.VerifyMetaDataIscreated("AutomateMe-MultiSelectBox")).to.be.true
        })

        it("Verify User can create CheckBoxList Metadata", () => {
            // Metadata.Open("/user/Metadata.action")
            // Metadata.WaitForPageToLoad()
            // Metadata.ClickOnLob("AUTOMATEME")
            Metadata.ClickOnAddMetadata()
            expect(Metadata.VerifyAddFieldDialogAppears()).to.be.true
            Metadata.EnterFieldName("AutomateMe-CheckBoxList")
            Metadata.SelectFieldType(3)
            Metadata.IsItRequiredCheckBox(false)
            Metadata.ClickOnSave()
            expect(Metadata.VerifyMetaDataIscreated("AutomateMe-CheckBoxList")).to.be.true
        })

        it("Verify User can create Number Metadata", () => {
            // Metadata.Open("/user/Metadata.action")
            // Metadata.WaitForPageToLoad()
            // Metadata.ClickOnLob("AUTOMATEME")
            Metadata.ClickOnAddMetadata()
            expect(Metadata.VerifyAddFieldDialogAppears()).to.be.true
            Metadata.EnterFieldName("AutomateMe-Number")
            Metadata.SelectFieldType(4)
            Metadata.IsItRequiredCheckBox(false)
            Metadata.ClickOnSave()
            expect(Metadata.VerifyMetaDataIscreated("AutomateMe-Number")).to.be.true
        })

        it("Verify User can create Dollar Amount Metadata", () => {
            // Metadata.Open("/user/Metadata.action")
            // Metadata.WaitForPageToLoad()
            // Metadata.ClickOnLob("AUTOMATEME")
            Metadata.ClickOnAddMetadata()
            expect(Metadata.VerifyAddFieldDialogAppears()).to.be.true
            Metadata.EnterFieldName("AutomateMe-DollarAmount")
            Metadata.SelectFieldType(5)
            Metadata.IsItRequiredCheckBox(true)
            Metadata.ClickOnSave()
            expect(Metadata.VerifyMetaDataIscreated("AutomateMe-DollarAmount")).to.be.true
        })

    })

    //
    // ────────────────────────────────────────────────────── XII ──────────
    //   :::::: C L E A N U P : :  :   :    :     :        :          :
    // ────────────────────────────────────────────────────────────────
    //

    describe.skip("CLEANUP DATA", () => {
        it("Verify User can delete TextField Metadata", () => {
            Metadata.Open("/user/Metadata.action")
            Metadata.WaitForPageToLoad()
            Metadata.ClickOnLob("AUTOMATEME")
            Metadata.DeleteMetadata("AutomateMe-TextField")
            Metadata.ConfirmDelete()
            expect(Metadata.VerifyMetaDataIsDeleted("AutomateMe-TextField")).to.be.false

        })

        it("Verify User can delete SelectBox Metadata", () => {
            Metadata.Open("/user/Metadata.action")
            Metadata.WaitForPageToLoad()
            Metadata.ClickOnLob("AUTOMATEME")
            Metadata.DeleteMetadata("AutomateMe-SelectBox")
            Metadata.ConfirmDelete()
            expect(Metadata.VerifyMetaDataIsDeleted("AutomateMe-SelectBox")).to.be.false
        })

        it("Verify User can delete MultiSelectBox Metadata", () => {
            Metadata.Open("/user/Metadata.action")
            Metadata.WaitForPageToLoad()
            Metadata.ClickOnLob("AUTOMATEME")
            Metadata.DeleteMetadata("AutomateMe-MultiSelectBox")
            Metadata.ConfirmDelete()
            expect(Metadata.VerifyMetaDataIsDeleted("AutomateMe-MultiSelectBox")).to.be.false
        })

        it("Verify User can delete CheckBoxList Metadata", () => {
            Metadata.Open("/user/Metadata.action")
            Metadata.WaitForPageToLoad()
            Metadata.ClickOnLob("AUTOMATEME")
            Metadata.DeleteMetadata("AutomateMe-CheckBoxList")
            Metadata.ConfirmDelete()
            expect(Metadata.VerifyMetaDataIsDeleted("AutomateMe-CheckBoxList")).to.be.false
        })

        it("Verify User can delete Number Metadata", () => {
            Metadata.Open("/user/Metadata.action")
            Metadata.WaitForPageToLoad()
            Metadata.ClickOnLob("AUTOMATEME")
            Metadata.DeleteMetadata("AutomateMe-Number")
            Metadata.ConfirmDelete()
            expect(Metadata.VerifyMetaDataIsDeleted("AutomateMe-Number")).to.be.false
        })

        it("Verify User can delete DollarAmount Metadata", () => {
            Metadata.Open("/user/Metadata.action")
            Metadata.WaitForPageToLoad()
            Metadata.ClickOnLob("AUTOMATEME")
            Metadata.DeleteMetadata("AutomateMe-DollarAmount")
            Metadata.ConfirmDelete()
            expect(Metadata.VerifyMetaDataIsDeleted("AutomateMe-DollarAmount")).to.be.false
        })

        it("Verify user can delete opportunity", () => {

            Opportunity.Open("/user/Home.action")
            SearchItem.SelectSearchItem(1)
            SearchItem.EnterSearchText("AngryBird-Opportunity")
            SearchItem.ClickOnGoButton()
            Opportunity.ClickOnSearchedOppo("AngryBird-Opportunity")
            Opportunity.ClickOnSearchedOppoEdit()
            CleanUp.ClickOnDeleteButton()
            SearchItem.SelectSearchItem(1)
            SearchItem.EnterSearchText("AngryBird-Opportunity")
            SearchItem.ClickOnGoButton()
            expect(CleanUp.VerifyOppoIsDeleted("AngryBird-Opportunity")).to.be.false

        })

        it("Verify user can delete Client Company", () => {

            Opportunity.Open("/user/Home.action")
            SearchItem.SelectSearchItem(2)
            SearchItem.EnterSearchText("CCompany-AngryBird-EDITED")
            SearchItem.ClickOnGoButton()
            Opportunity.ClickOnSearchedOppo("CCompany-AngryBird-EDITED")
            ClientCompany.ClickOnEditClientCompanyIcon()
            CleanUp.ClickOnDeleteClientCompany()
            SearchItem.SelectSearchItem(2)
            SearchItem.EnterSearchText("CCompany-AngryBird-EDITED")
            SearchItem.ClickOnGoButton()
            expect(CleanUp.VerifyClientCompanyIsDeleted("CCompany-AngryBird-EDITED")).to.be.false

        })



    })


    //<!------------------End of SUITE---------------->
})