const step2Element = require('../element.json');

import { browser, by, element } from 'protractor';
export class CheckoutStep2Page {

    deliveryPage() {
        let step2Button = element(by.buttonText(step2Element.locators.checkoutStep2.step2Button));
        browser.actions().mouseMove(step2Button).click().perform();
    }

    noSeletedCustmer() {
        let noCustomer = element(by.buttonText(step2Element.locators.checkoutStep2.noCustomer));
        browser.actions().mouseMove(noCustomer).click().perform();
    };
}

