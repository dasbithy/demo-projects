const loginElement = require('../element.json');

import { browser, by, element } from 'protractor';

export class LoginPage {

  logInAsStagingUser() {
    browser.get(loginElement.locators.loginPage.armorStaging);
    element(by.css(loginElement.locators.loginPage.userName)).sendKeys(loginElement.locators.loginInput.usernameStaging);
    element(by.css(loginElement.locators.loginPage.password)).sendKeys(loginElement.locators.loginInput.passwordStaging);
    let loginButton = element(by.buttonText(loginElement.locators.loginPage.loginButton));
    browser.actions().mouseMove(loginButton).click().perform();
  }

  selectBusiness() {
    element.all(by.css(loginElement.locators.loginPage.marketTypeBusiness)).each((marketTypeBusiness) => {
      marketTypeBusiness.all(by.tagName(loginElement.locators.loginPage.businessSelection)).each((businessSelect, businessSelectIndex) => {
        if (businessSelectIndex === 1) {
          businessSelect.getText().then(function () {
            businessSelect.click();
          });
        }
      });
    });
  }
}

