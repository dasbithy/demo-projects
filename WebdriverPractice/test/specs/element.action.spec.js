import HomePage from '../pages/home.page'
import TestData from '../data/input.data'
describe("Test element action ",()=>{

it("will use expect from chai assertion" ,()=>{
    HomePage.caseTest();
    HomePage.clickOnLink(1);
    expect(browser.getUrl()).equal('http://the-internet.herokuapp.com/abtest');

})
it("Get text",()=>{
    HomePage.caseTest();
    expect(HomePage.getSpecificChildElement(1)).equal('A/B Testing');
})
xit("will check the selected checkbox",()=>{
    //Will return true or false whether or not an <option> or <input> element of type checkbox or radio is currently selected.
    HomePage.caseTest();
    HomePage.clickOnLink(6);
    HomePage.clickCheckbox(1)
    expect(HomePage.checkbox(1).isSelected()).equal(true);
})
xit("will uncheck the selected checkbox",()=>{
    //Will return true or false whether or not an <option> or <input> element of type checkbox or radio is currently selected
    HomePage.clickCheckbox(1)
    expect(HomePage.checkbox(1).isSelected()).equal(false);
})
it("Will set value ",()=>{
    HomePage.caseTest();
    HomePage.clickOnLink(21);
    HomePage.enterUsername(TestData.login.username);
    HomePage.enterpassword(TestData.login.password);
    browser.pause(6000);
    assert.equal(`Julia`, HomePage.username.getValue()) 
    assert.equal(`Password`, HomePage.password.getValue()) 
})

xit("Will clear the value from the username and password ",()=>{
    HomePage.username.click();
    HomePage.username.clearValue();
    assert.equal(``, HomePage.username.getValue()) 

})
xit("Will scroll the footer  ",()=>{
    HomePage.caseTest();
    HomePage.pageHeader.isDisplayed();
    HomePage.scrollPageFooter();
    assert.equal(true, HomePage.pageFooter.isDisplayedInViewport()) 
   browser.pause(60000);
})
xit("will switch to another windows",()=>{
    HomePage.caseTest();
    HomePage.clickOnLink(33);
    browser.pause(60000);
    HomePage.clickToLink();
    browser.switchWindow(`${browser.options.baseUrl}windows/new`)
    assert.equal(true, HomePage.newWindow.isDisplayed()) 
    assert.equal(true, HomePage.newWindow.isExisting()) 
    assert.equal('New Window', HomePage.newWindow.getText()) 
   browser.pause(60000);
})
xit("will use iframe write text",()=>{
    HomePage.caseTest();
    HomePage.clickOnLink(22);
    HomePage.clickframs(2);
    browser.pause(30000);
    HomePage.iframe.waitForDisplayed()
    browser.switchToFrame(HomePage.iframe)
    HomePage.sendTextInIframe('This is the text in the iframe body')
    assert.equal('This is the text in the iframe body', HomePage.iframebody.getText())
    browser.pause(60000);
})
xit("drag and drop ",()=>{
    HomePage.caseTest();
    HomePage.clickOnLink(10);
    HomePage.dragColumnAtoB();
    browser.pause(60000);
})
xit("Drop down ",()=>{
    HomePage.caseTest();
    HomePage.clickOnLink(11);
    HomePage.dropdownOption();
    assert.equal(true, HomePage.dropdownOption1.isSelected())
    browser.pause(60000);
})
it("Alert message for javascript ",()=>{
    HomePage.caseTest();
    HomePage.clickOnLink(29);
    browser.pause(6000);
    HomePage.clickJavascriptAlert(1);
    browser.pause(6000);
    assert.equal('I am a  Alert', browser.getAlertText())
    
})
xit("will accept alert ",()=>{
    browser.acceptAlert();
    assert.equal('You successfuly clicked an alert', HomePage.getAlertResult())

})
xit("will wait for enable elemennt ",()=>{
    HomePage.caseTest();
    HomePage.clickOnLink(13);
    HomePage.clickToEnable();
    HomePage.inputEnablefield.waitForEnabled(4000);
    assert.equal(true, HomePage.inputEnablefield.isEnabled())

})
xit("will wait for enable elemennt ",()=>{
    HomePage.caseTest();
    HomePage.clickOnLink(13);
    HomePage.clickToEnable();
    HomePage.inputEnablefield.waitForEnabled(4000);
    assert.equal(true, HomePage.inputEnablefield.isEnabled())

})
xit("will wait untile the element is fullfilled the condition",()=>{
    HomePage.caseTest();
    HomePage.clickOnLink(13);
    HomePage.clickPageButton();
    browser.waitUntil(()=>{
        HomePage.pageButton.getText()=== 'Add'
    },6000,"It's gone!")
    browser.pause(9000);
   assert.equal('Add', HomePage.pageButton.getText())

})


})