const login = require('../../locator.js')

class LoginPage {
    loginTestSite() {

        browser.url('/')

    }
    loginToSystem() {
        const usertype = $(login.locators.loginPage.userType)
        const domain = $(login.locators.loginPage.domain)
        const username=$(login.locators.loginPage.username)
        const password=$(login.locators.loginPage.password)
        const loginButton=$(login.locators.loginPage.loginButton)

        usertype.click()
        username.setValue(login.locators.loginSetValue.username);  
        password.setValue(login.locators.loginSetValue.password);
        domain.selectByIndex(4);
        loginButton.click()

    }
}
module.exports = new LoginPage();