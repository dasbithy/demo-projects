class HomePage {
   get pageHeader() { return $('h1.heading') }
   get subHeading() { return $("h2")}
   get newWindow() { return $("h3")}
   get pageFooter() {return $('#page-footer')}
   get parent() {return $('ul')}
   get childElements() {return this.parent.$$('li')}
   firstLink(index){return $(`ul li:nth-child(${index}) a`)}
  checkbox(index) {return $(`#checkboxes input:nth-child(${index})`)}
  get username(){return $('#username')}
  get password(){return $('#password')}
  get clickHere(){return $('.example a')}
  nestedFrames (index){return $(`#content .example ul li:nth-child(${index}) a`)}
  get iframebody() {return $('#tinymce')}
  get iframe() {return $('#mce_0_ifr')}
  get columnA() {return $('#column-a')}
  get columnB() {return $('#column-b')}
  get dropdown() {return $('#dropdown')}
  get dropdownOption1() {return $('#dropdown option:nth-child(2)')}
  javascriptAert(index){return $(`.example li:nth-child(${index}) button`)}
  get alertResult() {return $('.example #result')}
  get enableButton() {return $('#input-example button')}
  get inputEnablefield() {return $('#input-example input')}
  get pageButton(){return $('#checkbox-example button')}

clickPageButton(){
   this.pageButton.waitForDisplayed();
   this.pageButton.click();
}
  clickToEnable(){
     this.enableButton.waitForDisplayed();
     this.enableButton.click();
  }
  getAlertResult(){
     this.alertResult.waitForDisplayed()
     return this.alertResult.getText();
  }
  clickJavascriptAlert(index){
     this.javascriptAert(index).waitForDisplayed()
     this.javascriptAert(index).click();
  }
  dropdownOption(){
   this.dropdown.waitForDisplayed()
   this.dropdown.click();
   this.dropdownOption1.waitForDisplayed();
   this.dropdownOption1.click();
   
}
  dragColumnAtoB(){
     this.columnA.waitForDisplayed()
     this.columnA.dragAndDrop(this.columnB)
  }
  sendTextInIframe(text){
     this.iframebody.waitForDisplayed();
     this.iframebody.clearValue();
     this.iframebody.click();
     this.iframebody.keys(text);

  }
  clickframs(index){
     this.nestedFrames(index).waitForDisplayed();
     this.nestedFrames(index).click();
  }
  clickToLink(){
     this.clickHere.waitForDisplayed();
     this.clickHere.click();
  }
  /**
   * scroll to the the page footer 
   */
  scrollPageFooter(){
     this.pageFooter.scrollIntoView();
  }

 caseTest() {
      //const assert = require('assert');
      browser.url('/')
   }
   specificChildElement(index){
    { return this.parent.$(`li:nth-child(${index})`) }
   }
   getSpecificChildElement(index){
      this.specificChildElement(index).waitForDisplayed();
    return this.specificChildElement(index).getText();
   }
   getLiText(){
      this.childElements.filter((element)=>{
         console.log(element);
      })
   }
   clickOnLink(index){
      this.firstLink(index).isDisplayed() 
         this.firstLink(index).click();
   }
   clickCheckbox(index){
      this.checkbox(index).waitForDisplayed();
      this.checkbox(index).click();
   }
  enterUsername(text){
      this.username.waitForDisplayed();
      this.username.setValue(text)
   }
/**
 * @param {text}
 */
enterpassword(text){
   this.password.waitForDisplayed();
   this.password.setValue(text)
}
}

module.exports = new HomePage();
