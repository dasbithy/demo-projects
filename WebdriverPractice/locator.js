module.exports={
    locators:{
        loginPage:{
            username:"#usernameVisible",
            userType:"#usernameType",
            password:"#password",
            domain:"#domain",
            loginButton:"#loginButton"
        
        },
        loginSetValue:{
            username:"test",
            password:"test"
        },
        add:{
            headerItem:"#helpmenu",
            navigate:"#navigation ul li:nth-child(3) a"

        },
        clientCompanyPage:{
            clientName:"#clientName",
            existingClient:"input[type='radio'][value='No']"

            
        }
    }
}