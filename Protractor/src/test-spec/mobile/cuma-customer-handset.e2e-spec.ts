import { CumaPage } from '../../pages/cuma.page';
import { LoginPage } from '../../pages/login.page';
import { MobileHandsetPage } from '../../pages/mobile.handset.page';
import { CheckoutStep1Page } from '../../pages/mobile.checkout-steps1.page';
import { CheckoutStep2Page } from '../../pages/mobile.checkout-steps2.page';
import { CheckoutStep4Page } from '../../pages/mobile.checkout-steps4.page';
import { CheckoutStep5Page } from '../../pages/mobile.checkout-steps5.page';

import { browser, logging } from 'protractor';

describe(" Mobile flow with handset and Subscription", () => {

  const cumaPage: CumaPage = new CumaPage();
  const loginPage: LoginPage = new LoginPage();
  const mobileHandsetPage: MobileHandsetPage = new MobileHandsetPage();
  const checkoutStep1Page: CheckoutStep1Page = new CheckoutStep1Page();
  const checkoutStep2Page: CheckoutStep2Page = new CheckoutStep2Page();
  const checkoutStep4Page: CheckoutStep4Page = new CheckoutStep4Page();
  const checkoutStep5Page: CheckoutStep5Page = new CheckoutStep5Page();

  it("Should login in the system as Staging user", () => {
    loginPage.logInAsStagingUser();
  });

  it("Should select Handset and go to the detail page", () => {
    mobileHandsetPage.selectMobileFlow();
    mobileHandsetPage.handsetSelection();
    mobileHandsetPage.checkHandsetPrice();
    mobileHandsetPage.addToCart();
  });

  it("Should go to the checkout process", () => {
    checkoutStep1Page.creatCart();
    checkoutStep2Page.deliveryPage();
    checkoutStep2Page.noSeletedCustmer();
  });

  it("Should select contact from Cuma", () => {
    cumaPage.customerSearch();
    cumaPage.customerSelect();
  });
  it("Should create order", () => {
    checkoutStep4Page.cumaCustomer();
    checkoutStep5Page.createOrder();
  });
});
