const handsetElement = require('../element.json');
import { browser, by, element } from 'protractor';

export class MobileHandsetPage {

    selectMobileFlow() {
        const mobile = element(by.id(handsetElement.locators.handsetPage.flowtypeMobile));
        browser.actions().mouseMove(mobile).click().perform();
    }

    selectHandset() {
        let handset = element(by.css(handsetElement.locators.handsetPage.handset));
        browser.actions().mouseMove(handset).click().perform();
    }

    checkHandsetPrice() {
        //This function is not done yet so after that i will refector it
        element.all(by.css(".accordion-item")).each((elem1) => {
            elem1.all(by.css(".accordion-nav")).each((elem2) => {
                elem2.all(by.css(".font-weight-bold")).each((elem4) => {
                    elem4.getText().then((text) => {
                        let a = text;
                        let b = "240,00";
                        expect(a).toEqual(b);
                    })
                })
            })
        })
    }

    handsetSelection() {
        element.all(by.css(handsetElement.locators.handsetPage.handsetList)).each((handsetWrapper, handsetWrapperIndex) => {
            if (handsetWrapperIndex === 0) {
                handsetWrapper.all(by.css(handsetElement.locators.handsetPage.handsetItem)).each((handsetItem, handsetItemIndex) => {
                    if (handsetItemIndex === 0) {
                        handsetItem.all(by.css(handsetElement.locators.handsetPage.handsetName)).each((handsetName, handsetNameIndex) => {
                            if (handsetNameIndex === 0) {
                                handsetName.all(by.tagName(handsetElement.locators.handsetPage.handsetSelect)).each((handsetSelect) => {
                                    browser.actions().mouseMove(handsetSelect).click().perform();
                                });
                            }
                        });
                    }
                });
            }
        });
    }

    handsetSearch = function () {
        element.all(by.css(handsetElement.locators.handsetPage.handsetSearchContainer)).each((handsetSearchContainer) => {
            handsetSearchContainer.all(by.css(handsetElement.locators.handsetPage.handsetSearchInputContainer)).each((handsetInputContainer, handsetInputContainerIndex) => {
                browser.actions().mouseMove(handsetInputContainer).click().perform();
                handsetInputContainer.all(by.css(handsetElement.locators.handsetPage.handsetSearchField)).sendKeys(handsetElement.locators.handsetPage.handsetSearchInput);
            });
        });

        element.all(by.css(handsetElement.locators.handsetPage.handsetSearchSuggestionContainer)).each((handsetSearchSuggestionContainer, suggestionContainerIndex) => {
            if (suggestionContainerIndex === 0) {
                handsetSearchSuggestionContainer.all(by.css(handsetElement.locators.handsetPage.handsetSearchSuggestionItem)).each((handsetSearchSuggestionItem, handsetSearchSuggestionItemIndex) => {
                    if (handsetSearchSuggestionItemIndex === 0) {
                        browser.actions().mouseMove(handsetSearchSuggestionItem).click().perform();
                    }
                });
            }
        });
    }

    mobileSubscription() {
        element.all(by.css(handsetElement.locators.handsetPage.handsetSelectSubscriptionItem)).each((handsetSubscriptionItem) => {
            handsetSubscriptionItem.all(by.css(handsetElement.locators.handsetPage.handsetSelectSubscriptionWithDiscount)).each((handsetSubscriptionDiscount, handsetSubscriptionDiscountIndex) => {
                if (handsetSubscriptionDiscountIndex === 3) {
                    handsetSubscriptionDiscount.all(by.css(handsetElement.locators.handsetPage.handsetSelectSubscriptionRadioButton)).each((handsetSelectSubscriptionButton, handsetSelectSubscriptionButtonIndex) => {
                        if (handsetSelectSubscriptionButtonIndex === 0) {
                            handsetSelectSubscriptionButton.all(by.css(handsetElement.locators.handsetPage.handsetSelectSubscription)).each(function (handsetSelectSubscription) {
                                browser.actions().mouseMove(handsetSelectSubscription).click().perform();
                            });
                        }
                    });
                }
            });
        });

    };

    selectCredit() {
        let creditButton = element(by.buttonText(handsetElement.locators.handsetPage.handsetSelectCredit));
        browser.actions().mouseMove(creditButton).click().perform();
    }

    selectCreditForILT() {
        let addCredit = element(by.buttonText(handsetElement.locators.handsetPage.handsetSelectCredit));

        for (let i = 0; i < 13;) {
            browser.actions().mouseMove(addCredit).click().perform();
            i++;
        }
    }

    addToCart() {
        let cartButton = element(by.id(handsetElement.locators.handsetPage.handsetAddToCart));
        browser.actions().mouseMove(cartButton).click().perform();
    }
}

