const cumaElement = require('../element.json');
import { browser, by, element } from 'protractor';

export class CumaPage {

    selectCuma() {
        let cuma = element(by.css(cumaElement.locators.cumaPage.cuma));
        browser.actions().mouseMove(cuma).click().perform();
    }

    customerSearch() {
        //zipcode search 
        element(by.css(cumaElement.locators.cumaPage.zipCode)).sendKeys(cumaElement.locators.cumaPage.zipCodeNumber);
        //zipcode search 
        element(by.css(cumaElement.locators.cumaPage.houseNumber)).sendKeys(cumaElement.locators.cumaPage.houseNumberInput);
        let searchButton = element(by.buttonText(cumaElement.locators.cumaPage.search));
        browser.actions().mouseMove(searchButton).click().perform();
    }

    customerSelect() {
        element.all(by.css(cumaElement.locators.cumaPage.cumaCustomerListTable)).each((cumaCustomerListTable) => {
            cumaCustomerListTable.all(by.tagName(cumaElement.locators.cumaPage.cumaCustomerList)).each((cumaCustomerList, cumaCustomerListIndex) => {
                if (cumaCustomerListIndex === 5) {
                    cumaCustomerList.all(by.tagName(cumaElement.locators.cumaPage.cumaCustomerSelection)).each((cumaCustomerSearchButton) => {
                        cumaCustomerSearchButton.getText().then((Buttontext) => {
                            if (Buttontext === 'Kies') {
                                cumaCustomerSearchButton.click();
                            }
                        });

                    });
                }
            });
        });

    };

    businessCustomerSelect() {
        element.all(by.css(cumaElement.locators.cumaPage.cuma_customer_list_table)).each((cumaCustomerListTable) => {
            cumaCustomerListTable.all(by.tagName(cumaElement.locators.cumaPage.cumaCustomerList)).each((cumaCustomerList, cumaCustomerListIndex) => {

                if (cumaCustomerListIndex === 3) {
                    cumaCustomerList.all(by.tagName(cumaElement.locators.cumaPage.cumaCustomerSelection)).each((cumaCustomerSearchButton) => {
                        cumaCustomerSearchButton.getText().then((Buttontext) => {
                            if (Buttontext === 'Kies') {
                                cumaCustomerSearchButton.click();
                            }
                        });
                    });
                }
            });
        });

    };
}

