const cartOverviewPage = require('../element.json');
import { browser, by, element } from 'protractor';

export class CheckoutStep1Page {

    creatCart() {
        let step1Button = element(by.buttonText(cartOverviewPage.locators.checkoutStep1.step1Button));
        browser.actions().mouseMove(step1Button).click().perform();
    }
}

