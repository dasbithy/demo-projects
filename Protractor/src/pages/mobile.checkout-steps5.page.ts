const step5element = require('../element.json');
import { browser, by, element } from 'protractor';

export class CheckoutStep5Page {

    createOrder() {
        //Klant gaat akkoord met:
        let checkbox1 = element(by.css(step5element.locators.checkoutStep5.checkbox1));
        browser.actions().mouseMove(checkbox1).click().perform();
        //cps_email
        let checkbox2 = element(by.id(step5element.locators.checkoutStep5.checkbox2));
        browser.actions().mouseMove(checkbox2).click().perform();

        let step5Button = element(by.buttonText(step5element.locators.checkoutStep5.step5Button));
        browser.actions().mouseMove(step5Button).click().perform();
    }
}
