const step4Element = require('../element.json');
import { browser, by, element, WebElement } from 'protractor';

export class CheckoutStep4Page {

    cumaCustomer() {
        //cuma customer information update
        browser.waitForAngular();
        element(by.css(step4Element.locators.checkoutStep4.customerUpdateEmail)).clear();
        element(by.css(step4Element.locators.checkoutStep4.customerUpdateEmail)).sendKeys(step4Element.locators.checkoutStep4.customerEmail);
        element(by.css(step4Element.locators.checkoutStep4.customerBank)).sendKeys(step4Element.locators.checkoutStep4.customerBankNumber);
        //Customer id selection 
        element.all(by.css(step4Element.locators.checkoutStep4.customerIdSelectOption)).each((customerIdSelect) => {
            browser.actions().mouseMove(customerIdSelect).click().perform();

            element.all(by.tagName(step4Element.locators.checkoutStep4.customerIdOption)).each((customerIdOption) => {
                customerIdOption.getText().then((text) => {
                    if (text === 'Paspoort') {
                        customerIdOption.click();
                    }
                });
            });
        });
        //customer id information 
        element(by.css(step4Element.locators.checkoutStep4.customerIdPassport)).sendKeys(step4Element.locators.checkoutStep4.customerIdPassportNumber);
        element(by.css(step4Element.locators.checkoutStep4.customerIdIssue)).sendKeys(step4Element.locators.checkoutStep4.customerIdIssueDate);
        element(by.css(step4Element.locators.checkoutStep4.customerIdExpire)).sendKeys(step4Element.locators.checkoutStep4.customerIdExpireDate);

        let step4Button = element(by.buttonText(step4Element.locators.checkoutStep4.customerUpdate));
        browser.actions().mouseMove(step4Button).click().perform();
    }

    cumaBusinessCustomer() {
        //business requester selection
        element.all(by.css(step4Element.locators.checkoutStep4.businessCumaRequesterForms)).each((requesterForms, requesterFormsIndex) => {
            if (requesterFormsIndex === 0) {
                requesterForms.all(by.tagName(step4Element.locators.checkoutStep4.businessCumaRequesterFormsDiv)).each((requesterFormsDiv, requesterFormsDivIndex) => {
                    if (requesterFormsDivIndex === 0) {
                        requesterFormsDiv.all(by.css(step4Element.locators.checkoutStep4.businessCumaRequester)).each((businessRequester, requesterIndex) => {
                            if (requesterIndex === 0) {
                                businessRequester.all(by.css(step4Element.locators.checkoutStep4.businessCumaRequesterSelection)).each((businessRequesterSelection) => {
                                    browser.actions().mouseMove(businessRequesterSelection).click().perform();
                                });
                            }
                        });
                    }
                });
            }
        });
    }
}
